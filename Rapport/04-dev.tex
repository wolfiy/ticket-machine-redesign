% !TEX root = report.tex

\chapter{Réalisation}
\section{Structure du code}
Le programme a été réalisé en suivant les principes du patron de conception \textit{Model-View-Controller}.

\subsection{Classes non MVC}
En plus des classes MVC, quelques ajouts ont été faits afin de faciliter le fonctionnement du programme.

\subsubsection{Cart}
Le panier d'achat est modélisé par la classe \codeword{Cart}. Celle-ci permet d'ajouter ou d'enlever une ou plusieurs
instances de \codeword{Ticket} dans la commande en cours

\subsubsection{Enums}
Pour faciliter certaines opérations comme l'ouverture de vue ou la définition des types de tickets, des enumérateurs ont
été définis:

\begin{itemize}
    \item \codeword{Lang}, pour les langues du programme,
    \item \codeword{PriceTier}, pour la catégorie de prix,
    \item \codeword{TicketType}, pour le type de ticket,
    \item \codeword{ViewType}, pour les différentes vues.
\end{itemize}

\subsubsection{Ticket}
La classe \codeword{Ticket} permet de représenter un ticket en particulier. Celui-ci est doté d'un numéro
d'identification unique, d'un prix, d'une description, d'une validité, d'un type (\textit{t+}, \textit{Paris Visite},
\dots) et d'une catégorie de prix (standard, réduit, extra).

\subsection{MVC - Models}
Pour ce projet, seule une classe modèle \textit{TicketMachineModel} a été nécessaire. Celle-ci contient notamment le
panier d'achat, les descriptions et les différentes combinaisons possible de tickets.

\subsubsection{CartModel}
Le panier d'achat contient l'entièreté des billets actuellement dans la commande. Il se sert des classes
\codeword{Ticket} et \codeword{Cart}.

\subsection{MVC - Views}
Afin de faciliter l'implémentation des vues et éviter la duplication de code inutile (gestion des langue, création du
résumé de commande, \dots), la notion d'héritage a souvent été utilisée.

\subsubsection{TicketMachineEmptyBaseView}
La classe \codeword{TicketMachineEmptyBaseView} sert de base à toutes les autres vues du projet. C'est dans celle-ci que
la gestion des langues est faite à l'aide de la méthode suivante, apprise dans le module ICT 120.

\begin{lstlisting}
// Lors d'un clic sur les drapeaux du header
private void Lang_Click(object? sender, EventArgs e)
{
    if (sender is not PictureBox langSelection)
        throw new ArgumentException("Sender is invalid!", 
                                    nameof(sender));

    Lang lang = langSelection.Name switch
    {
        "picGerman" => Lang.DE,
        "picEnglish" => Lang.EN,
        "picFrench" => Lang.FR,
        "picItalian" => Lang.IT,
        "picSpanish" => Lang.ES,
        _ => throw new ArgumentException(
                "Unknown language.",
                nameof(langSelection)
            )
    };

    Controller.SetLang(lang);
}
\end{lstlisting}

Une fois le bouton de langue cliqué, la méthode \codeword{SetLang} du controlleur appelle \codeword{UpdateLanguage} dans
une boucle qui rafraichi le contenu de toutes les vues.

\begin{lstlisting}
// Rafraichissement des controls de la vue
public virtual void UpdateLanguage(ResourceManager rm)
{
    foreach (Control c in Controls)
    {
        if (rm.GetString(c.Name) != null)
            c.Text = rm.GetString(c.Name);
    }
}
\end{lstlisting}

Cette classe s'occupe aussi de mettre à jour l'affichage de la date et de l'heure en fonction de la locale courante.
Finalement, un bouton de retour à l'accueil et les méthodes d'ouverture / de fermeture de la fenêtre y sont également
définies.

\subsubsection{TicketMachineHomeView}
La page d'accueil est restée relativement simple: deux zones cliquables clairement définies par un sobre fond gris. Afin
de faciliter l'implémentation, et plus particulièrement l'ajout des bords arrondis, le fond gris a été implémenté en
utilisant une \codeword{PictureBox}.

\subsubsection{TicketMachineTicketSelectionView}
Tout comme la page d'accueil, l'écran de sélection du type de ticket est simple et les différents éléments ont été
clairement définis.

Ici, le même principe que pour l'accueil a été utilisé pour créer les fond gris. Au vu du nombre d'éléments qui se
devaient d'être cliquables (fond gris, nom du ticket, description), la gestion des événements s'est vu être légèrement
\og lourde \fg{}.

\subsubsection{TicketMachineBaseWithSummary}
Cette classe est, tout comme la \textit{empty base view}, utilisée à des fins d'héritage uniquement. Cette base permet
de créer un résumé de la commande en cours.

Premièrement, la méthode \codeword{CreateSummaryLabels} itère sur toutes les combinaisons de tickets possibles et, si
cette combinaison est présente dans le panier d'achat, créé un nouveau \codeword{Label} correspondant.

Ensuite, la méthode \codeword{DisplaySummaryLabels} permet d'ajouter ces éléments aux contrôles existants et de les
afficher.

\begin{lstlisting}
protected void DisplaySummaryLabels()
{
    // If a label was already present, remove it
    foreach (Label l in _activeSummarylabels)
        Controls.Remove(l);

    // Reset active labels
    _activeSummarylabels.Clear();

    // Add newly created labels
    foreach (Label l in CreateSummaryLabels())
    {
        Controls.Add(l);
        _activeSummarylabels.Add(l);
    }
}
\end{lstlisting}

A noter que les \codeword{Labels} du résumé sont cliquables et permettent de retourner à la page d'achat correspondante.

\subsubsection{TicketMachineSimpleTicketOrder / TicketMachineDoubleTicketOrder}
Le fonctionnement de ces deux classes est extrêmement similaire: la seule réelle différence étant que la version \og
double \fg{} permet de sélectionner le nombre de jours de validité; elle hérite donc de la première.

Afin de réduire la duplication de code, le constructeur de la version \og simple \fg{} prend en argument le type de
ticket que la vue représente et affiche les textes correspondants. Cela permet d'éviter d'avoir trois classes
supplémentaires qui n'auraient pas de sens à être.

\begin{lstlisting}
public TicketMachineSimpleTicketOrder(TicketType type) : this()
{
    InitializeComponent();
    _activeSummarylabels = [];
    _type = type;

    // Adjusts the title depending on the type of ticket this
    // view is used for
    lblMainTitle.Name = type switch
    {
        TicketType.T_PLUS => "lblTicketTypeTPlus",
        TicketType.PARIS_VISITE => "lblTicketTypeParisVisite",
        TicketType.DISNEYLAND => "lblTicketTypeDisney",
        TicketType.AIRPORT => "lblTicketTypeAirport",
        _ => throw new ArgumentException("Unknown label.", 
                                            nameof(type))
    };

    // ...
}
\end{lstlisting}

De plus, lors d'un clic sur les boutons d'ajout ou de suppression de tickets, le résumé et le nombre d'éléments
sélectionnés sont mis à jour. Il est aussi possible de revenir à l'écran de sélection de billets grâce au bouton \og
retour \fg{}.

\subsubsection{TicketMachineTicketOrderView}
Contrairement aux précédentes vues, celle-ci est très simple. Elle ne sert qu'à ouvrir la vue correspondant au moyen de
payement ou d'annuler la commande en cours.

Lorsque l'utilisateur sélectionne son moyen de payement, les textes affichés à l'écran changent et la personne est alors
invitée à suivre les instructions du terminal de payement. Si l'utilisateur revient en arrière, ou si la commande est
annulée, les textes reprennent leur contenu originel.

\subsection{MVC - Controllers}
Le programme ne possède qu'un seul contrôleur. Celui-ci s'occupe de communiquer avec le modèle, d'ouvrir les
différentes vues, définir la locale et de gérer le panier d'achat.

La création du résumé ne se basant que sur le type et la catégorie des tickets, le code s'occupant de récupérer les
informations peut paraitre quelque peu intimidant.

\begin{lstlisting}
public (TicketType type, TicketTier tier, string desc, double price, int count) 
    GetFullTicketDetails(TicketType type, TicketTier tier)
{
    string desc = GetTicketName(type, tier);
    double price = GetTicketPrice(type, tier);
    int count = GetTicketCount(type, tier);

    return (type, tier, desc, price, count);
}
\end{lstlisting}

Cette méthode retourne un tuple, ce qui permet de regrouper différentes valeurs de types différents sans devoir créer un
objet dédié.

Pour définir le nom du ticket, la catégorie de prix est ajoutée à la fin du type, si applicable.

\begin{lstlisting}
public string GetTicketName(TicketType type, TicketTier tier)
{
    string name = _model.TicketNames[type];

    string suffix = tier switch
    {
        TicketTier.REDUCED => 
            $"({Manager.GetString("lblTicketCategoryReduced")})",
        TicketTier.EXTRA => 
            $"({Manager.GetString("lblTicketCategoryExtra")})",
        _ => string.Empty
    };

    return $"{name} {suffix}";
}
\end{lstlisting}

\section{.NET 8}
Plutôt que d'utiliser le vieillissant \textit{.NET Framework}, ce projet a été réalisé à l'aide de \textit{.NET 8}, ce
qui permis de faciliter certaines opérations en utilisant de nouvelles fonctionnalités du langage C\#. 

\subsection{Pattern matching}
Le \textit{pattern matching} permet de tester une expression. Cela fut particulièrement utile dans les événements pour
s'assurer que le \textit{sender} soit bel et bien du type attendu. 

En voici quelques exemples.

\begin{lstlisting}
private void Lang_Click(object? sender, EventArgs e)
{
    // Only accept PictureBox senders
    if (sender is not PictureBox langSelection)
        throw new ArgumentException("Sender is invalid!", 
                                    nameof(sender));

    // ...
}
\end{lstlisting}

\begin{lstlisting}
private void Home_Click(object? sender, EventArgs e)
{
    // Do not open the home view if already on the home view
    if (this is TicketMachineHomeView) 
        return;
    
    // ...
}
\end{lstlisting}

\subsection{Switches}
Les \textit{switch expressions} permettent d'obtenir un certain résultat en utilisant du \textit{pattern matching} sur
une entrée. Dans les cas où un \codeword{switch} est utilisé pour donner une valeur à une variable, cette version
améliorée permet de radicalement réduire la longueur du code:

\begin{lstlisting}
// Switch classique
Lang lang;
switch (langSelection.Name) 
{
    case "picGerman":
        lang = Lang.DE;
        break;
    case "picEnglish":
        lang = Lang.EN;
        break;
    case "picFrench":
        lang = Lang.EN;
        break;
    // ...
    default:
        throw new ArgumentException("Unknown language",
                                    nameof(langSelection));
}
\end{lstlisting}

\begin{lstlisting}
// "Switch expression"
Lang lang = langSelection.Name switch
{
    "picGerman" => Lang.DE,
    "picEnglish" => Lang.EN,
    "picFrench" => Lang.FR,
    "picItalian" => Lang.IT,
    "picSpanish" => Lang.ES,
    _ => throw new ArgumentException("Unknown language.",
                                    nameof(langSelection))
};
\end{lstlisting}

\subsection{Expression bodied-members}
Pour les méthodes qui ne font que retourner une valeur, il est possible de raccourcir leur corps en utilisant les
expressions, par exemple:

\begin{lstlisting}
// Expression
public int GetNbDaysParisVisit() => _model.NbDaysParisVisite;
\end{lstlisting}

\begin{lstlisting}
// Block 
public int GetNbDaysParisVisite()
{
    return _model.NbDaysParisVisite;
}
\end{lstlisting}

\subsection{Nullable}
Certains types ne sont sensés jamais être nul, mais il est possible d'autoriser cela en ajoutant \codeword{?} au type.
Les règles et normes de codage de \textit{Microsoft} conseillent de déclarer le \textit{sender} dans les événements
comme \textit{nullable}. 

Il en va de même pour les attributs sans valeur par défaut référencés après l'instanciation d'un objet (par exemple le
\codeword{Controller} des vues ou du modèle).

\section{Changements et autres remarques}
Entre la création de la maquette et l'écriture des dernières lignes de code, quelques changements on eut lieu, faute de
complexité ou par volonté d'améliorer l'expérience utilisateur.

\begin{itemize}
    \item Le fond gris du résumé de commande est absent;
    \item Un bouton d'annulation a été ajouté à la page de payement;
    \item En plus des pièces, la page de payement indiques les billets acceptés;
    \item Le \textit{layout} de la page de payement est différent: le résumé est à gauche et les indications sont sur la
    droite;
    \item Au lieu de créer une vue spécifique à chaque moyen de payement, la dernière vue s'adapte au choix de
    l'utilisateur.
\end{itemize}

Comme le programme ne possède pas de champs ouverts, les REGEX n'ont pas été utilisées.

Finalement, quelques éléments ont contribué à une conception écologique:

\begin{itemize}
    \item La suppression de code dupliqué rend le projet plus léger;
    \item Le recourt à la documentation de \textit{Microsoft} plutôt qu'à l'intelligence artificielle réduit grandement
    l'impact des recherches;
    \item Les \textit{commits} ont été \textit{push} par lot plutôt qu'individuellement;
    \item Le rapport a été rédigé en \LaTeX, ce qui élimina le besoin d'utiliser des logiciels de traitement de texte
    lourds comme \textit{LibreOffice} ou \textit{Word}.
\end{itemize}