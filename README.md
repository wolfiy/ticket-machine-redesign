# Ticket Machine
A Windows Form train ticket machine simulator. Tries to improve RATP's machines. Part of the P_UX class at ETML.

## Building
This project was made in C# (.NET 8), with Windows Form.

## Features
### Mockup
A mockup of the program's GUI was made and can be found either in the report or as a `.fig` file at the root of this
repository.

### Internationalization
This program is available in English, French, German, Italian and Spanish.

### MVC
The ticket machine simulator was written following the MVC design pattern.