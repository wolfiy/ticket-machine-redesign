﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TicketMachine.Resources.Languages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class EN {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal EN() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TicketMachine.Resources.Languages.EN", typeof(EN).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go back.
        /// </summary>
        internal static string btnBack {
            get {
                return ResourceManager.GetString("btnBack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buy another ticket.
        /// </summary>
        internal static string btnBuyMore {
            get {
                return ResourceManager.GetString("btnBuyMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel order.
        /// </summary>
        internal static string btnCancelOrder {
            get {
                return ResourceManager.GetString("btnCancelOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pay with card.
        /// </summary>
        internal static string btnCard {
            get {
                return ResourceManager.GetString("btnCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pay by cash.
        /// </summary>
        internal static string btnCash {
            get {
                return ResourceManager.GetString("btnCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        internal static string btnConfirm {
            get {
                return ResourceManager.GetString("btnConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm and pay.
        /// </summary>
        internal static string btnConfirmPay {
            get {
                return ResourceManager.GetString("btnConfirmPay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to cancel your order?.
        /// </summary>
        internal static string cancelOrderConfirm {
            get {
                return ResourceManager.GetString("cancelOrderConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subtitle.
        /// </summary>
        internal static string DefaultMainSubtitle {
            get {
                return ResourceManager.GetString("DefaultMainSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Main title.
        /// </summary>
        internal static string DefaultMainTitle {
            get {
                return ResourceManager.GetString("DefaultMainTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cart.
        /// </summary>
        internal static string lblCartTitle {
            get {
                return ResourceManager.GetString("lblCartTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        internal static string lblCartTotal {
            get {
                return ResourceManager.GetString("lblCartTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to day.
        /// </summary>
        internal static string lblDay {
            get {
                return ResourceManager.GetString("lblDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to days.
        /// </summary>
        internal static string lblDays {
            get {
                return ResourceManager.GetString("lblDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Navigo pass.
        /// </summary>
        internal static string lblHomeNavigo {
            get {
                return ResourceManager.GetString("lblHomeNavigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to What do you want to do?.
        /// </summary>
        internal static string lblHomeSubtitle {
            get {
                return ResourceManager.GetString("lblHomeSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buy a ticket.
        /// </summary>
        internal static string lblHomeTicket {
            get {
                return ResourceManager.GetString("lblHomeTicket", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        internal static string lblHomeTitle {
            get {
                return ResourceManager.GetString("lblHomeTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accepted cards.
        /// </summary>
        internal static string lblPaymentCards {
            get {
                return ResourceManager.GetString("lblPaymentCards", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summary.
        /// </summary>
        internal static string lblPaymentCart {
            get {
                return ResourceManager.GetString("lblPaymentCart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accepted coins and banknotes.
        /// </summary>
        internal static string lblPaymentCoins {
            get {
                return ResourceManager.GetString("lblPaymentCoins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refer to the terminal.
        /// </summary>
        internal static string lblPaymentDescriptionCard {
            get {
                return ResourceManager.GetString("lblPaymentDescriptionCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please insert coins.
        /// </summary>
        internal static string lblPaymentDescriptionCash {
            get {
                return ResourceManager.GetString("lblPaymentDescriptionCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose your payment method.
        /// </summary>
        internal static string lblPaymentSubtitle {
            get {
                return ResourceManager.GetString("lblPaymentSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please refer to the payment terminal.
        /// </summary>
        internal static string lblPaymentSubtitleTerminal {
            get {
                return ResourceManager.GetString("lblPaymentSubtitleTerminal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment.
        /// </summary>
        internal static string lblPaymentTitle {
            get {
                return ResourceManager.GetString("lblPaymentTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pay with card.
        /// </summary>
        internal static string lblPaymentTitleCard {
            get {
                return ResourceManager.GetString("lblPaymentTitleCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pay by cash.
        /// </summary>
        internal static string lblPaymentTitleCash {
            get {
                return ResourceManager.GetString("lblPaymentTitleCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to reduced.
        /// </summary>
        internal static string lblReduced {
            get {
                return ResourceManager.GetString("lblReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of days.
        /// </summary>
        internal static string lblTicketCategoryDays {
            get {
                return ResourceManager.GetString("lblTicketCategoryDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Extra.
        /// </summary>
        internal static string lblTicketCategoryExtra {
            get {
                return ResourceManager.GetString("lblTicketCategoryExtra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reduced fare.
        /// </summary>
        internal static string lblTicketCategoryReduced {
            get {
                return ResourceManager.GetString("lblTicketCategoryReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Standard fare.
        /// </summary>
        internal static string lblTicketCategoryStandard {
            get {
                return ResourceManager.GetString("lblTicketCategoryStandard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to One-way ticket to the airport.
        /// </summary>
        internal static string lblTicketDescriptionAirport {
            get {
                return ResourceManager.GetString("lblTicketDescriptionAirport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to How long is your stay?.
        /// </summary>
        internal static string lblTicketDescriptionDays {
            get {
                return ResourceManager.GetString("lblTicketDescriptionDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An easy access to Disneyland Paris.
        /// </summary>
        internal static string lblTicketDescriptionDisneyLand {
            get {
                return ResourceManager.GetString("lblTicketDescriptionDisneyLand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bikes, pets.
        /// </summary>
        internal static string lblTicketDescriptionExtra {
            get {
                return ResourceManager.GetString("lblTicketDescriptionExtra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To enjoy all public transport during your stay.
        /// </summary>
        internal static string lblTicketDescriptionParisVisite {
            get {
                return ResourceManager.GetString("lblTicketDescriptionParisVisite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Children, students, AVS.
        /// </summary>
        internal static string lblTicketDescriptionReduced {
            get {
                return ResourceManager.GetString("lblTicketDescriptionReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Standard ticket for adults.
        /// </summary>
        internal static string lblTicketDescriptionStandard {
            get {
                return ResourceManager.GetString("lblTicketDescriptionStandard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Underground, Parisian RER, bus, tramway.
        /// </summary>
        internal static string lblTicketDescriptionTPlus {
            get {
                return ResourceManager.GetString("lblTicketDescriptionTPlus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tickets.
        /// </summary>
        internal static string lblTicketOrderCount {
            get {
                return ResourceManager.GetString("lblTicketOrderCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Days.
        /// </summary>
        internal static string lblTicketOrderDays {
            get {
                return ResourceManager.GetString("lblTicketOrderDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose the desired ticket type.
        /// </summary>
        internal static string lblTicketSelectionSubtitle {
            get {
                return ResourceManager.GetString("lblTicketSelectionSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ticket selection.
        /// </summary>
        internal static string lblTicketSelectionTitle {
            get {
                return ResourceManager.GetString("lblTicketSelectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airport ticket.
        /// </summary>
        internal static string lblTicketTypeAirport {
            get {
                return ResourceManager.GetString("lblTicketTypeAirport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disneyland ticket.
        /// </summary>
        internal static string lblTicketTypeDisney {
            get {
                return ResourceManager.GetString("lblTicketTypeDisney", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Paris Visite ticket.
        /// </summary>
        internal static string lblTicketTypeParisVisite {
            get {
                return ResourceManager.GetString("lblTicketTypeParisVisite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to t+ ticket.
        /// </summary>
        internal static string lblTicketTypeTPlus {
            get {
                return ResourceManager.GetString("lblTicketTypeTPlus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity: {0} onwards.
        /// </summary>
        internal static string lblTicketValidity {
            get {
                return ResourceManager.GetString("lblTicketValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity:.
        /// </summary>
        internal static string lblValidity {
            get {
                return ResourceManager.GetString("lblValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An Error occurred..
        /// </summary>
        internal static string txtError {
            get {
                return ResourceManager.GetString("txtError", resourceCulture);
            }
        }
    }
}
