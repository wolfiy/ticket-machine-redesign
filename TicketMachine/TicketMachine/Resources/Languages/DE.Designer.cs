﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TicketMachine.Resources.Languages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class DE {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DE() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TicketMachine.Resources.Languages.DE", typeof(DE).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zurück.
        /// </summary>
        internal static string btnBack {
            get {
                return ResourceManager.GetString("btnBack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaufen Sie ein weiteres Ticket.
        /// </summary>
        internal static string btnBuyMore {
            get {
                return ResourceManager.GetString("btnBuyMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bestellung stornieren.
        /// </summary>
        internal static string btnCancelOrder {
            get {
                return ResourceManager.GetString("btnCancelOrder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mit Karte bezahlen.
        /// </summary>
        internal static string btnCard {
            get {
                return ResourceManager.GetString("btnCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barzahlung.
        /// </summary>
        internal static string btnCash {
            get {
                return ResourceManager.GetString("btnCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bestätigen.
        /// </summary>
        internal static string btnConfirm {
            get {
                return ResourceManager.GetString("btnConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bestätigen und bezahlen.
        /// </summary>
        internal static string btnConfirmPay {
            get {
                return ResourceManager.GetString("btnConfirmPay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Möchten Sie Ihre Bestellung wirklich stornieren?.
        /// </summary>
        internal static string cancelOrderConfirm {
            get {
                return ResourceManager.GetString("cancelOrderConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bestätigen und bezahlen.
        /// </summary>
        internal static string lblButtonConfirmPay {
            get {
                return ResourceManager.GetString("lblButtonConfirmPay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warenkorb.
        /// </summary>
        internal static string lblCartTitle {
            get {
                return ResourceManager.GetString("lblCartTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Titel.
        /// </summary>
        internal static string lblCartTotal {
            get {
                return ResourceManager.GetString("lblCartTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tag.
        /// </summary>
        internal static string lblDay {
            get {
                return ResourceManager.GetString("lblDay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tage.
        /// </summary>
        internal static string lblDays {
            get {
                return ResourceManager.GetString("lblDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subtitle.
        /// </summary>
        internal static string lblDefaultMainSubtitle {
            get {
                return ResourceManager.GetString("lblDefaultMainSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Untertitel.
        /// </summary>
        internal static string lblDefaultMainTitle {
            get {
                return ResourceManager.GetString("lblDefaultMainTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Navigo pass.
        /// </summary>
        internal static string lblHomeNavigo {
            get {
                return ResourceManager.GetString("lblHomeNavigo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Was möchten Sie tun?.
        /// </summary>
        internal static string lblHomeSubtitle {
            get {
                return ResourceManager.GetString("lblHomeSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kauf ein Ticket.
        /// </summary>
        internal static string lblHomeTicket {
            get {
                return ResourceManager.GetString("lblHomeTicket", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Willkommen.
        /// </summary>
        internal static string lblHomeTitle {
            get {
                return ResourceManager.GetString("lblHomeTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Akzeptierte Karten.
        /// </summary>
        internal static string lblPaymentCards {
            get {
                return ResourceManager.GetString("lblPaymentCards", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zusammenfassung.
        /// </summary>
        internal static string lblPaymentCart {
            get {
                return ResourceManager.GetString("lblPaymentCart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Akzeptierte Münzen und Banknoten.
        /// </summary>
        internal static string lblPaymentCoins {
            get {
                return ResourceManager.GetString("lblPaymentCoins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beziehen Sie sich auf das Terminal.
        /// </summary>
        internal static string lblPaymentDescriptionCard {
            get {
                return ResourceManager.GetString("lblPaymentDescriptionCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bitte werfen Sie Münzen ein.
        /// </summary>
        internal static string lblPaymentDescriptionCash {
            get {
                return ResourceManager.GetString("lblPaymentDescriptionCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wähle deine Zahlungsmethode.
        /// </summary>
        internal static string lblPaymentSubtitle {
            get {
                return ResourceManager.GetString("lblPaymentSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bitte beachten Sie das Zahlungsterminal.
        /// </summary>
        internal static string lblPaymentSubtitleTerminal {
            get {
                return ResourceManager.GetString("lblPaymentSubtitleTerminal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zahlung.
        /// </summary>
        internal static string lblPaymentTitle {
            get {
                return ResourceManager.GetString("lblPaymentTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mit Karte bezahlen.
        /// </summary>
        internal static string lblPaymentTitleCard {
            get {
                return ResourceManager.GetString("lblPaymentTitleCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barzahlung.
        /// </summary>
        internal static string lblPaymentTitleCash {
            get {
                return ResourceManager.GetString("lblPaymentTitleCash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sparticket.
        /// </summary>
        internal static string lblReduced {
            get {
                return ResourceManager.GetString("lblReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anzahl der Tage.
        /// </summary>
        internal static string lblTicketCategoryDays {
            get {
                return ResourceManager.GetString("lblTicketCategoryDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Extra.
        /// </summary>
        internal static string lblTicketCategoryExtra {
            get {
                return ResourceManager.GetString("lblTicketCategoryExtra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sparticket.
        /// </summary>
        internal static string lblTicketCategoryReduced {
            get {
                return ResourceManager.GetString("lblTicketCategoryReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Standard-Tarif.
        /// </summary>
        internal static string lblTicketCategoryStandard {
            get {
                return ResourceManager.GetString("lblTicketCategoryStandard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to One-Way-Ticket zum Flughafen.
        /// </summary>
        internal static string lblTicketDescriptionAirport {
            get {
                return ResourceManager.GetString("lblTicketDescriptionAirport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wie viele Tage bleiben Sie?.
        /// </summary>
        internal static string lblTicketDescriptionDays {
            get {
                return ResourceManager.GetString("lblTicketDescriptionDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ein einfacher Zugang zum Disneyland Paris.
        /// </summary>
        internal static string lblTicketDescriptionDisneyLand {
            get {
                return ResourceManager.GetString("lblTicketDescriptionDisneyLand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fahrräder, Haustiere.
        /// </summary>
        internal static string lblTicketDescriptionExtra {
            get {
                return ResourceManager.GetString("lblTicketDescriptionExtra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to So können Sie während Ihres Aufenthalts alle öffentlichen Verkehrsmittel nutzen.
        /// </summary>
        internal static string lblTicketDescriptionParisVisite {
            get {
                return ResourceManager.GetString("lblTicketDescriptionParisVisite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kinder, Studenten, AVS.
        /// </summary>
        internal static string lblTicketDescriptionReduced {
            get {
                return ResourceManager.GetString("lblTicketDescriptionReduced", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Standardticket für Erwachsene.
        /// </summary>
        internal static string lblTicketDescriptionStandard {
            get {
                return ResourceManager.GetString("lblTicketDescriptionStandard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to U-Bahn, Pariser RER, Bus, Straßenbahn.
        /// </summary>
        internal static string lblTicketDescriptionTPlus {
            get {
                return ResourceManager.GetString("lblTicketDescriptionTPlus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tickets.
        /// </summary>
        internal static string lblTicketOrderCount {
            get {
                return ResourceManager.GetString("lblTicketOrderCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tage.
        /// </summary>
        internal static string lblTicketOrderDays {
            get {
                return ResourceManager.GetString("lblTicketOrderDays", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wählen Sie die gewünschte Ticketart.
        /// </summary>
        internal static string lblTicketSelectionSubtitle {
            get {
                return ResourceManager.GetString("lblTicketSelectionSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ticketauswahl.
        /// </summary>
        internal static string lblTicketSelectionTitle {
            get {
                return ResourceManager.GetString("lblTicketSelectionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flughafen Ticket.
        /// </summary>
        internal static string lblTicketTypeAirport {
            get {
                return ResourceManager.GetString("lblTicketTypeAirport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disneyland Ticket.
        /// </summary>
        internal static string lblTicketTypeDisney {
            get {
                return ResourceManager.GetString("lblTicketTypeDisney", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Paris Visite Ticket.
        /// </summary>
        internal static string lblTicketTypeParisVisite {
            get {
                return ResourceManager.GetString("lblTicketTypeParisVisite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to t+ Ticket.
        /// </summary>
        internal static string lblTicketTypeTPlus {
            get {
                return ResourceManager.GetString("lblTicketTypeTPlus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gültigkeit: ab {0}.
        /// </summary>
        internal static string lblTicketValidity {
            get {
                return ResourceManager.GetString("lblTicketValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gültigkeit:.
        /// </summary>
        internal static string lblValidity {
            get {
                return ResourceManager.GetString("lblValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ein Fehler ist aufgetreten..
        /// </summary>
        internal static string txtError {
            get {
                return ResourceManager.GetString("txtError", resourceCulture);
            }
        }
    }
}
