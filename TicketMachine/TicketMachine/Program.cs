/// ETML
/// Author: Sebastien TILLE
/// Date: April 18th, 2024

using TicketMachine.Controllers;
using TicketMachine.Models;
using TicketMachine.Views;

namespace TicketMachine
{
    /// <summary>
    /// Launches the Ticket Machine program.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();

            // Model
            TicketMachineModel model = new();

            // Views
            Dictionary<string, TicketMachineEmptyBaseView> views = new()
            {
                { "home", new TicketMachineHomeView() },
                { "selection", new TicketMachineTicketSelectionView() },
                { "tplus", new TicketMachineSimpleTicketOrder(Utils.TicketType.TPlus) },
                { "disneyland", new TicketMachineSimpleTicketOrder(Utils.TicketType.Disneyland) },
                { "airport", new TicketMachineSimpleTicketOrder(Utils.TicketType.Airport) },
                { "parisvisite", new TicketMachineDoubleTicketOrder(Utils.TicketType.ParisVisite) },
                { "payment", new TicketMachinePaymentView() }
            };

            // Controller
            TicketMachineController controller = new(model, views);
            
            // Open home window
            Application.Run(views["home"]);
        }
    }
}