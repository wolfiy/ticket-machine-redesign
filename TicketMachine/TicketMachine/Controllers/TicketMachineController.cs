﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 25th, 2024

using System.Globalization;
using System.Resources;
using TicketMachine.Models;
using TicketMachine.Utils;

namespace TicketMachine.Controllers
{
    /// <summary>
    /// Main controller of the ticket machine program.
    /// </summary>
    public class TicketMachineController
    {
        /// <summary>
        /// Contains the ticket machine's data.
        /// </summary>
        private readonly TicketMachineModel _model;

        /// <summary>
        /// List of all views.
        /// </summary>
        private readonly Dictionary<string, TicketMachineEmptyBaseView> _views;

        /// <summary>
        /// Current language of the program.
        /// </summary>
        public Lang Lang { get; set; }

        /// <summary>
        /// Current culture of the program.
        /// </summary>
        public CultureInfo Culture { get; set; }

        /// <summary>
        /// Resource manager used to handle texts.
        /// </summary>
        public ResourceManager ResourceManager { get; set; }

        /// <summary>
        /// Initializes a new <see cref="TicketMachineController"/>. By default,
        /// language is set to English and culture to en-US.
        /// </summary>
        /// <param name="model">'M' component of MVC.</param>
        /// <param name="views">'V' component of MVC.</param>
        public TicketMachineController(TicketMachineModel model, Dictionary<string, TicketMachineEmptyBaseView> views)
        {
            // Link model and views
            _model = model;
            _views = views;

            // MVC bootstrap
            _model.Controller = this;
            foreach (var view in _views.Values)
                view.Controller = this;

            // Default culture and language
            Culture = new CultureInfo("en-US");
            Lang = Lang.EN;
            ResourceManager = SetLang(Lang.EN);

            // Apply culture and language to views
            foreach (var view in views.Values)
            {
                view.UpdateLanguage(ResourceManager);
                view.UpdateDateTime();
            }
        }

        #region Language and locale
        /// <summary>
        /// Sets the language of the program.
        /// </summary>
        /// <param name="lang">Language to be used.</param>
        /// <returns>A resource manager containing all strings in the 
        /// specified language.</returns>
        /// <exception cref="ArgumentException">If the language does not exist 
        /// or is unsupported.</exception>
        public ResourceManager SetLang(Lang lang)
        {
            // Set language and locale
            Lang = lang;
            SetCulture(lang);

            // Initialize new resource manager
            ResourceManager = lang switch
            {
                Lang.DE => new ResourceManager(typeof(Resources.Languages.DE)),
                Lang.EN => new ResourceManager(typeof(Resources.Languages.EN)),
                Lang.ES => new ResourceManager(typeof(Resources.Languages.ES)),
                Lang.FR => new ResourceManager(typeof(Resources.Languages.FR)),
                Lang.IT => new ResourceManager(typeof(Resources.Languages.IT)),
                _ => throw new ArgumentException("Language is unsupported.", 
                                                 nameof(lang))
            };

            // Apply language and locale
            foreach (var view in _views.Values)
            {
                view.UpdateLanguage(ResourceManager);
                view.UpdateDateTime();
            }

            return ResourceManager;
        }

        /// <summary>
        /// Sets the culture of the program.
        /// </summary>
        /// <param name="lang">Language from which to gather the culture.
        /// </param>
        /// <returns>The corresponding culture info.</returns>
        /// <exception cref="ArgumentException">If the language does not exist 
        /// or is unsupported.</exception>
        private CultureInfo SetCulture(Lang lang)
        {
            // Get culture from language
            Culture = lang switch
            {
                Lang.DE => new CultureInfo("de-DE"),
                Lang.EN => new CultureInfo("en-US"),
                Lang.FR => new CultureInfo("fr-FR"),
                Lang.IT => new CultureInfo("it-IT"),
                Lang.ES => new CultureInfo("es-ES"),
                _ => throw new ArgumentException("Culture is unsupported", 
                                                 nameof(lang))
            };

            return Culture;
        }
        #endregion

        #region Window management
        /// <summary>
        /// Open a specific view.
        /// </summary>
        /// <param name="caller">View from which the method was called.</param>
        /// <param name="pane">The type of view to open.</param>
        /// <returns>False if the view could not be opened, true otherwise.
        /// </returns>
        public bool OpenView(TicketMachineEmptyBaseView caller,
                             ViewType pane)
        {
            // Closes the calling view
            caller.Close();

            // Attempting to get the requested view
            bool found = _views.TryGetValue(pane.ToString().ToLower(), 
                                            out var view);

            // Stop if no view is found
            if (!found)
                return false;

            // Open view
            view?.Open();

            return true;
        }

        /// <summary>
        /// Open a view given a ticket type.
        /// </summary>
        /// <returns>True if the view has been successfuly opened, false 
        /// otherwise.</returns>
        public bool OpenView(TicketType type)
        {
            // Close all views
            foreach (TicketMachineEmptyBaseView v in Application.OpenForms)
                v.Close();

            // Attempting to get the requested view
            bool found = _views.TryGetValue(type.ToString().ToLower(),
                                            out var view);

            // Stop if no view is found
            if (!found)
                return false;

            // Open view
            view?.Open();

            return true;
        }
        #endregion

        #region Ticket / cart management
        /// <summary>
        /// Adds a ticket to the cart.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>The index, in the cart, of the new ticket.</returns>
        public int AddTicket(TicketType type, PriceTier tier, DateTime date)
        {
            // By default, tickets are valid for one day
            DateTime endDate = date;

            // 'Paris Visite' tickets can be valid for multiple days
            if (type is TicketType.ParisVisite)
                endDate = date.AddDays(GetNbDaysParisVisite());

            // Add ticket to cart
            _model.Cart.AddTicket(_model.NextId, type, tier, date, endDate);

            // Increment used IDs
            return ++_model.NextId;
        }

        /// <summary>
        /// Removes the last added ticket of a given type and price tier.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>True if the ticket has been successfully removed, false
        /// otherwise.</returns>
        public bool RemoveTicket(TicketType type, PriceTier tier)
            => _model.Cart.RemoveLast(type, tier);

        /// <summary>
        /// Clears the cart.
        /// </summary>
        public void ClearCart() => _model.Cart.Clear();
        #endregion

        #region Getters / setters
        /// <summary>
        /// Gets the current date in the dd mmmm yyyy format.
        /// </summary>
        /// <returns>A string containing the current date.</returns>
        public string GetDate() => 
            DateTime.Now.Date.ToString(Culture.DateTimeFormat.LongDatePattern, 
                                       Culture);

        /// <summary>
        /// Gets the current time in the HH:mm format.
        /// </summary>
        /// <returns>A string containing the current time.</returns>
        public string GetTime() => 
            DateTime.Now.ToString(Culture.DateTimeFormat.ShortTimePattern, 
                                  Culture);

        /// <summary>
        /// Gets the current number of days setting for 'Paris Visite' tickets.
        /// </summary>
        /// <returns>The number of days for 'Paris Visite' tickets.</returns>
        public int GetNbDaysParisVisite() => _model.NbDaysParisVisite;

        /// <summary>
        /// Sets the number of days setting for 'Paris Visite' tickets.
        /// </summary>
        /// <param name="n">New number of days.</param>
        public void SetNbDaysParisVisite(int n) => _model.NbDaysParisVisite = n;

        /// <summary>
        /// Gets the number of tickets of a given type and price tier currently 
        /// in the cart.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>The number of tickets currently in the cart.</returns>
        public int GetTicketCount(TicketType type, PriceTier tier)
            => _model.Cart.GetTicketCount(type, tier);

        /// <summary>
        /// Gets all possible ticket combinations.
        /// </summary>
        /// <returns>A list of all ticket combinations.</returns>
        public List<(TicketType type, PriceTier tier)> GetTicketCombinations()
            => _model.TicketCombinations;

        /// <summary>
        /// Gets the details of a ticket of a given type and price tier.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>A tuple containing the ticket's type, price tier, name, 
        /// price and total count in the cart.</returns>
        public (TicketType type, PriceTier tier, string name, double price, 
                int count)
            GetTicketDetails(TicketType type, PriceTier tier)
        {
            string name = GetTicketName(type, tier);
            double price = GetTicketPrice(type, tier);
            int count = GetTicketCount(type, tier);

            return (type, tier, name, price, count);
        }

        /// <summary>
        /// Gets the name of a ticket given its type and price tier. If a ticket
        /// is of wither reduced or extra price tier, a suffix is added.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>The ticket's full name.</returns>
        /// <exception cref="ArgumentException">If the type does not exist.
        /// </exception>
        private string GetTicketName(TicketType type, PriceTier tier)
        {
            bool found = _model.TicketNames.TryGetValue(type, out var name);

            if (!found)
                throw new ArgumentException("Type does not exist!", nameof(type));

            // Single day, standard ticket need no more information
            if (tier is PriceTier.Standard && type is not TicketType.ParisVisite)
                return $"{name}";

            // Add price tier
            string suffix = tier switch
            {
                PriceTier.Reduced =>
                    $"({ResourceManager.GetString("lblReduced")})",
                PriceTier.Extra =>
                    $"({ResourceManager.GetString("lblTicketCategoryExtra")})",
                _ => string.Empty
            };

            // Add number of days if type is 'Paris Visite'
            if (type is TicketType.ParisVisite)
            {
                int n = GetNbDaysParisVisite();
                suffix += $", {n} ";
                suffix += n > 1 ? ResourceManager.GetString("lblDays") 
                                : ResourceManager.GetString("lblDay");
            }

            return $"{name} {suffix}";
        }

        /// <summary>
        /// Gets a ticket's price given its type and price tier.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <returns>The price of one ticket.</returns>
        private double GetTicketPrice(TicketType type, PriceTier tier)
        {
            return new Ticket(-1, type, tier, DateTime.Now, DateTime.Now).Price;
        }

        /// <summary>
        /// Gets a string from the current resource manager.
        /// </summary>
        /// <param name="str">String to look for.</param>
        /// <returns>The corresponding string in the current resource manager.
        /// </returns>
        public string? GetString(string str) => ResourceManager.GetString(str);
        #endregion
    }
}
