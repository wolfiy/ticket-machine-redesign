﻿/// ETML
/// Author: Sebastien TILLE
/// Date: April 25th, 2024

using TicketMachine.Controllers;
using TicketMachine.Utils;

namespace TicketMachine.Models
{
    /// <summary>
    /// Database placeholder.
    /// </summary>
    public class TicketMachineModel
    {
        /// <summary>
        /// Base name of 'Airport' tickets.
        /// </summary>
        public const string TICKET_DESC_AIRPORT = "Airport";

        /// <summary>
        /// Base name of 'Disneyland' tickets.
        /// </summary>
        public const string TICKET_DESC_DISNEYLAND = "Disneyland";

        /// <summary>
        /// Base name of 'Paris Visite' tickets.
        /// </summary>
        public const string TICKET_DESC_PARIS_VISITE = "Paris Visite";

        /// <summary>
        /// Base name of 't+' tickets.
        /// </summary>
        public const string TICKET_DESC_T_PLUS = "t+";

        /// <summary>
        /// Number of days setting for 'Paris Visite' tickets.
        /// </summary>
        public int NbDaysParisVisite {  get; set; }

        /// <summary>
        /// Labels used to display the order summary.
        /// </summary>
        public List<Label> SummaryLabels { get; set; }

        /// <summary>
        /// Next available ID to uniquely identify tickets.
        /// </summary>
        public int NextId { get; set; }

        /// <summary>
        /// A list of all the existing tickets.
        /// </summary>
        public List<(TicketType type, PriceTier tier)> 
            TicketCombinations { get; }

        /// <summary>
        /// Cart used to store the customer's order.
        /// </summary>
        public Cart Cart { get; set; }

        /// <summary>
        /// Tickets names.
        /// </summary>
        public Dictionary<TicketType, string> TicketNames { get; }

        /// <summary>
        /// Main controller.
        /// </summary>
        public TicketMachineController Controller { get; set; }

        /// <summary>
        /// Initializes a new <see cref="TicketMachineModel"/>.
        /// </summary>
        public TicketMachineModel()
        {
            SummaryLabels = [];
            Cart = new Cart();
            NextId = 0;
            NbDaysParisVisite = 1;

            // Fill existing ticket combinations
            TicketCombinations = [
                // "t+" tickets
                (TicketType.TPlus, PriceTier.Standard),
                (TicketType.TPlus, PriceTier.Reduced),
                (TicketType.TPlus, PriceTier.Extra),

                // "Paris Visite" tickets
                (TicketType.ParisVisite, PriceTier.Standard),
                (TicketType.ParisVisite, PriceTier.Reduced),
                (TicketType.ParisVisite, PriceTier.Extra),

                // "Airport" tickets
                (TicketType.Airport, PriceTier.Standard),
                (TicketType.Airport, PriceTier.Reduced),
                (TicketType.Airport, PriceTier.Extra),

                // "Disneyland" tickets
                (TicketType.Disneyland, PriceTier.Standard),
                (TicketType.Disneyland, PriceTier.Reduced),
                (TicketType.Disneyland, PriceTier.Extra),
            ];

            // Fill hashset
            TicketNames = new Dictionary<TicketType, string>
            {
                { TicketType.TPlus, TICKET_DESC_T_PLUS },
                { TicketType.ParisVisite, TICKET_DESC_PARIS_VISITE },
                { TicketType.Disneyland, TICKET_DESC_DISNEYLAND },
                { TicketType.Airport, TICKET_DESC_AIRPORT }
            };
        }
    }
}
