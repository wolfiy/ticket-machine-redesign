﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

using TicketMachine.Utils;

namespace TicketMachine.Models
{
    /// <summary>
    /// Represent a ticket that can be purchased from the ticket machine.
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// Price of a standard 'Airport' ticket.
        /// </summary>
        private const double AIRPORT_STANDARD_PRICE = 1.5;

        /// <summary>
        /// Price of a reduced 'Airport' ticket.
        /// </summary>
        private const double AIRPORT_REDUCED_PRICE = 1;

        /// <summary>
        /// Price of an extra 'Airport' ticket.
        /// </summary>
        private const double AIRPORT_EXTRA_PRICE = 0.5;

        /// <summary>
        /// Price of a standard 'Disneyland' ticket.
        /// </summary>
        private const double DISNEYLAND_STANDARD_PRICE = 3;

        /// <summary>
        /// Price of a reduced 'Disneyland' ticket.
        /// </summary>
        private const double DISNEYLAND_REDUCED_PRICE = 2;

        /// <summary>
        /// Price of an extra 'Disneyland' ticket.
        /// </summary>
        private const double DISNEYLAND_EXTRA_PRICE = 1;

        /// <summary>
        /// Price of a standard 'Paris Visite' ticket.
        /// </summary>
        private const double PARIS_VISITE_STANDARD_PRICE = 4;

        /// <summary>
        /// Price of a reduced 'Paris Visite' ticket.
        /// </summary>
        private const double PARIS_VISITE_REDUCED_PRICE = 3.5;

        /// <summary>
        /// Price of an extra 'Paris Visite' ticket.
        /// </summary>
        private const double PARIS_VISITE_EXTRA_PRICE = 1;

        /// <summary>
        /// Price of a standard 't+' ticket.
        /// </summary>
        private const double TPLUS_STANDARD_PRICE = 2;

        /// <summary>
        /// Price of a reduced 't+' ticket.
        /// </summary>
        private const double TPLUS_REDUCED_PRICE = 1.5;

        /// <summary>
        /// Price of an extra 't+' ticket.
        /// </summary>
        private const double TPLUS_EXTRA_PRICE = 0.5;

        /// <summary>
        /// Id of the ticket.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Price of the ticket.
        /// </summary>
        public double Price { get; private set; }

        /// <summary>
        /// Description of the ticket.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Date from which the ticket is valid.
        /// </summary>
        public DateTime ValidityStartDate { get; set; }

        /// <summary>
        /// Date until which the ticket is valid.
        /// </summary>
        public DateTime ValidityEndDate { get; set; }

        /// <summary>
        /// Type of ticket.
        /// </summary>
        public TicketType Type { get; }

        /// <summary>
        /// Tier of the ticket (standard, reduced, extra).
        /// </summary>
        public PriceTier Tier { get; private set; }

        /// <summary>
        /// Initializes a new <see cref="Ticket"/>.
        /// </summary>
        /// <param name="type">Type of the ticket (t+, etc.).</param>
        /// <param name="tier">Tier of the ticket (standard, etc.).</param>
        public Ticket(int id, TicketType type, PriceTier tier, 
                      DateTime start, DateTime end)
        {
            Type = type;
            Tier = tier;
            ValidityStartDate = start;
            ValidityEndDate = end;
            Description = string.Empty;

            Price = (type, tier) switch
            {
                (TicketType.Airport, PriceTier.Standard) 
                    => AIRPORT_STANDARD_PRICE,
                (TicketType.Airport, PriceTier.Reduced) 
                    => AIRPORT_REDUCED_PRICE,
                (TicketType.Airport, PriceTier.Extra) 
                    => AIRPORT_EXTRA_PRICE,

                (TicketType.Disneyland, PriceTier.Standard) 
                    => DISNEYLAND_STANDARD_PRICE,
                (TicketType.Disneyland, PriceTier.Reduced) 
                    => DISNEYLAND_REDUCED_PRICE,
                (TicketType.Disneyland, PriceTier.Extra) 
                    => DISNEYLAND_EXTRA_PRICE,

                (TicketType.ParisVisite, PriceTier.Standard) 
                    => PARIS_VISITE_STANDARD_PRICE,
                (TicketType.ParisVisite, PriceTier.Reduced) 
                    => PARIS_VISITE_REDUCED_PRICE,
                (TicketType.ParisVisite, PriceTier.Extra) 
                    => PARIS_VISITE_EXTRA_PRICE,

                (TicketType.TPlus, PriceTier.Standard) 
                    => TPLUS_STANDARD_PRICE,
                (TicketType.TPlus, PriceTier.Reduced) 
                    => TPLUS_REDUCED_PRICE,
                (TicketType.TPlus, PriceTier.Extra) 
                    => TPLUS_EXTRA_PRICE,

                _ => throw new NotImplementedException(),
            };
        }
    }
}
