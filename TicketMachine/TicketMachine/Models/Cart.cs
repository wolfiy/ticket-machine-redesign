﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

using TicketMachine.Utils;

namespace TicketMachine.Models
{
    /// <summary>
    /// Represent a cart used to order a <see cref="Ticket"/>.
    /// </summary>
    public class Cart
    {
        /// <summary>
        /// Tickets currently in the cart.
        /// </summary>
        public List<Ticket> Tickets { get; set; }

        /// <summary>
        /// Contains the count of each types of tickets.
        /// </summary>
        public Dictionary<(TicketType, PriceTier), string> Descriptions { get; set; }

        /// <summary>
        /// Initializes a new, empty <see cref="Cart"/>.
        /// </summary>
        public Cart()
        {
            Tickets = [];
            Descriptions = [];
        }

        /// <summary>
        /// Adds a ticket to the cart.
        /// </summary>
        /// <param name="id">Unique identifire of the ticket.</param>
        /// <param name="type">Type of ticket.</param>
        /// <param name="tier">Tier of ticket.</param>
        public void AddTicket(int id, TicketType type, PriceTier tier, 
                              DateTime start, DateTime end)
        {
            Tickets.Add(new Ticket(id, type, tier, start, end));
        }

        /// <summary>
        /// Removes a ticket from the cart.
        /// </summary>
        /// <param name="id">ID of the ticket to remove.</param>
        /// <returns>True if the ticket has been correctly removed, false 
        /// otherwise.</returns>
        public bool RemoveTicket(int id)
        {
            try
            {
                var ticket = Tickets.Find(t => t.Id == id);
                Tickets.Remove(ticket);
                return true;
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine($"Could not find ticket with id '{id}'.");
            }

            return false;
        }

        /// <summary>
        /// Removes the last occurence of a given type of ticket. This method 
        /// should only be called from the ticket order view as a last resort
        /// when the ID cannot be found.
        /// </summary>
        /// <param name="type">Type of ticket.</param>
        /// <param name="tier">Tier of ticket.</param>
        /// <returns>True if it has been successfully removed, false otherwise.
        /// </returns>
        public bool RemoveLast(TicketType type, PriceTier tier)
        {
            try
            {
                var ticket = Tickets.FindLast(t => t.Type == type
                                          && t.Tier == tier);
                Tickets.Remove(ticket);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"No ticket was found. Type: '{type}', " +
                                  $"tier: '{tier}'");
            }

            return false;
        }

        /// <summary>
        /// Gets the number of tickets of a certain type and tier currently
        /// in the basket.
        /// </summary>
        /// <param name="type">Type of the ticket.</param>
        /// <param name="tier">Tier of the ticket.</param>
        /// <returns>The number of tickets of a given type currently in the
        /// cart.</returns>
        public int GetTicketCount(TicketType type, PriceTier tier)
            => Tickets.Count(t => t.Type == type && t.Tier == tier);

        /// <summary>
        /// Clears the cart.
        /// </summary>
        public void Clear()
        {
            Tickets.Clear();
        }
    }
}
