﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

namespace TicketMachine.Utils
{
    /// <summary>
    /// Tiers of tickets that can be purchased.
    /// </summary>
    public enum PriceTier
    {
        /// <summary>
        /// Standard ticket price tier.
        /// </summary>
        Standard,

        /// <summary>
        /// Reduced ticket price tier.
        /// </summary>
        Reduced,

        /// <summary>
        /// Extra ticket price tier.
        /// </summary>
        Extra
    }
}
