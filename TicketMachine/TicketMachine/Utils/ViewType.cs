﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 13th, 2024

namespace TicketMachine.Utils
{
    /// <summary>
    /// Views available to the program.
    /// </summary>
    public enum ViewType
    {
        /// <summary>
        /// Home view.
        /// </summary>
        Home,
        
        /// <summary>
        /// Ticket selection view.
        /// </summary>
        Selection,

        /// <summary>
        /// 't+' ticket order view.
        /// </summary>
        TPlus,

        /// <summary>
        /// 'Paris Visite' ticket order view.
        /// </summary>
        ParisVisite,

        /// <summary>
        /// 'Disneyland' ticket order view.
        /// </summary>
        Disneyland,

        /// <summary>
        /// 'Airport' ticket view.
        /// </summary>
        Airport,

        /// <summary>
        /// Payment view.
        /// </summary>
        Payment
    }
}
