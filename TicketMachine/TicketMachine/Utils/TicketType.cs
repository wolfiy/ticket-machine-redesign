﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

namespace TicketMachine.Utils
{
    /// <summary>
    /// Represents the different types of tickets available for purchase.
    /// </summary>
    public enum TicketType
    {
        /// <summary>
        /// 't+' ticket type.
        /// </summary>
        TPlus,

        /// <summary>
        /// 'Paris Visite' ticket type.
        /// </summary>
        ParisVisite,

        /// <summary>
        /// 'Disneyland' ticket type.
        /// </summary>
        Disneyland,

        /// <summary>
        /// 'Airport' ticket type.
        /// </summary>
        Airport
    }
}
