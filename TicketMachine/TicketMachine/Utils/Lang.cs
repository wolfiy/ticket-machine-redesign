﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 13th, 2024

namespace TicketMachine.Utils
{
    /// <summary>
    /// Languages into which the program has been translated.
    /// </summary>
    public enum Lang
    {
        /// <summary>
        /// German language.
        /// </summary>
        DE,

        /// <summary>
        /// English language.
        /// </summary>
        EN,

        /// <summary>
        /// French language.
        /// </summary>
        FR,

        /// <summary>
        /// Italian language.
        /// </summary>
        IT,

        /// <summary>
        /// Spanish language.
        /// </summary>
        ES 
    }
}
