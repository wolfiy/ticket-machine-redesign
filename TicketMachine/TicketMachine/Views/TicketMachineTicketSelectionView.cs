﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// This view displays the different available ticket types and lets the
    /// user open the associated ticket order view.
    /// </summary>
    public partial class TicketMachineTicketSelectionView 
                       : TicketMachineEmptyBaseView
    {
        /// <summary>
        /// Initializes a new <see cref="TicketMachineTicketSelectionView"/>.
        /// </summary>
        public TicketMachineTicketSelectionView()
        {
            InitializeComponent();

            picTicketType0.Click += TicketTypeBackground_Click;
            picTicketType1.Click += TicketTypeBackground_Click;
            picTicketType2.Click += TicketTypeBackground_Click;
            picTicketType3.Click += TicketTypeBackground_Click;

            lblTicketTypeAirport.Click += TicketTypeText_Click;
            lblTicketTypeDisney.Click += TicketTypeText_Click;
            lblTicketTypeParisVisite.Click += TicketTypeText_Click;
            lblTicketTypeTPlus.Click += TicketTypeText_Click;

            lblTicketDescriptionAirport.Click += TicketTypeText_Click;
            lblTicketDescriptionDisneyLand.Click += TicketTypeText_Click;
            lblTicketDescriptionParisVisite.Click += TicketTypeText_Click;
            lblTicketDescriptionTPlus.Click += TicketTypeText_Click;
        }

        #region Events
        /// <summary>
        /// Opens the correct ticket order view.
        /// </summary>
        /// <param name="sender">A ticket type card.</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the call was made by an
        /// invalid element.</exception>
        private void TicketTypeBackground_Click(object? sender, EventArgs e)
        {
            if (sender is not PictureBox background)
                throw new ArgumentException("Sender is invalid!",
                                            nameof(sender));
            
            ViewType that = background.Name switch
            {
                "picTicketType0" => ViewType.TPlus,
                "picTicketType1" => ViewType.ParisVisite,
                "picTicketType2" => ViewType.Disneyland,
                "picTicketType3" => ViewType.Airport,
                _ => throw new ArgumentException(nameof(background))
            };

            Controller.OpenView(this, that);
        }

        /// <summary>
        /// Opens the correct ticket order view.
        /// </summary>
        /// <param name="sender">A ticket type title.</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the call was made by an
        /// invalid element.</exception>
        private void TicketTypeText_Click(object? sender, EventArgs e)
        {
            if (sender is not Label text)
                throw new ArgumentException("Sender is invalid!",
                                            nameof(sender));

            ViewType that = text.Name switch
            {
                "lblTicketTypeAirport" => ViewType.Airport,
                "lblTicketDescriptionAirport" => ViewType.Airport,
                "lblTicketTypeDisney" => ViewType.Disneyland,
                "lblTicketDescriptionDisneyLand" => ViewType.Disneyland,
                "lblTicketTypeParisVisite" => ViewType.ParisVisite,
                "lblTicketDescriptionParisVisite" => ViewType.ParisVisite,
                "lblTicketTypeTPlus" => ViewType.TPlus,
                "lblTicketDescriptionTPlus" => ViewType.TPlus,
                _ => throw new ArgumentException(nameof(text))
            };

            Controller.OpenView(this, that);
        }
        #endregion
    }
}
