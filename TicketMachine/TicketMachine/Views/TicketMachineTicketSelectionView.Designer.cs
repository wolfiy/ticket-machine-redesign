﻿namespace TicketMachine.Views
{
    partial class TicketMachineTicketSelectionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblTicketSelectionSubtitle = new Label();
            lblTicketSelectionTitle = new Label();
            picTicketType3 = new PictureBox();
            picTicketType2 = new PictureBox();
            picTicketType1 = new PictureBox();
            picTicketType0 = new PictureBox();
            lblTicketTypeTPlus = new Label();
            lblTicketDescriptionTPlus = new Label();
            lblTicketDescriptionParisVisite = new Label();
            lblTicketTypeParisVisite = new Label();
            lblTicketDescriptionDisneyLand = new Label();
            lblTicketTypeDisney = new Label();
            lblTicketDescriptionAirport = new Label();
            lblTicketTypeAirport = new Label();
            ((System.ComponentModel.ISupportInitialize)picTicketType3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType0).BeginInit();
            SuspendLayout();
            // 
            // lblTicketSelectionSubtitle
            // 
            lblTicketSelectionSubtitle.AutoSize = true;
            lblTicketSelectionSubtitle.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketSelectionSubtitle.Location = new Point(48, 161);
            lblTicketSelectionSubtitle.Name = "lblTicketSelectionSubtitle";
            lblTicketSelectionSubtitle.Size = new Size(80, 24);
            lblTicketSelectionSubtitle.TabIndex = 13;
            lblTicketSelectionSubtitle.Text = "Subtitle";
            // 
            // lblTicketSelectionTitle
            // 
            lblTicketSelectionTitle.AutoSize = true;
            lblTicketSelectionTitle.Font = new Font("Arial", 38.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblTicketSelectionTitle.Location = new Point(42, 101);
            lblTicketSelectionTitle.Name = "lblTicketSelectionTitle";
            lblTicketSelectionTitle.Size = new Size(244, 60);
            lblTicketSelectionTitle.TabIndex = 12;
            lblTicketSelectionTitle.Text = "Main title";
            // 
            // picTicketType3
            // 
            picTicketType3.Cursor = Cursors.Hand;
            picTicketType3.Image = Resources.Assets.TicketType;
            picTicketType3.Location = new Point(53, 593);
            picTicketType3.Name = "picTicketType3";
            picTicketType3.Size = new Size(894, 103);
            picTicketType3.SizeMode = PictureBoxSizeMode.AutoSize;
            picTicketType3.TabIndex = 19;
            picTicketType3.TabStop = false;
            // 
            // picTicketType2
            // 
            picTicketType2.Cursor = Cursors.Hand;
            picTicketType2.Image = Resources.Assets.TicketType;
            picTicketType2.Location = new Point(53, 465);
            picTicketType2.Name = "picTicketType2";
            picTicketType2.Size = new Size(894, 103);
            picTicketType2.SizeMode = PictureBoxSizeMode.AutoSize;
            picTicketType2.TabIndex = 18;
            picTicketType2.TabStop = false;
            // 
            // picTicketType1
            // 
            picTicketType1.Cursor = Cursors.Hand;
            picTicketType1.Image = Resources.Assets.TicketType;
            picTicketType1.Location = new Point(53, 337);
            picTicketType1.Name = "picTicketType1";
            picTicketType1.Size = new Size(894, 103);
            picTicketType1.SizeMode = PictureBoxSizeMode.AutoSize;
            picTicketType1.TabIndex = 17;
            picTicketType1.TabStop = false;
            // 
            // picTicketType0
            // 
            picTicketType0.Cursor = Cursors.Hand;
            picTicketType0.Image = Resources.Assets.TicketType;
            picTicketType0.Location = new Point(53, 208);
            picTicketType0.Name = "picTicketType0";
            picTicketType0.Size = new Size(894, 103);
            picTicketType0.SizeMode = PictureBoxSizeMode.AutoSize;
            picTicketType0.TabIndex = 16;
            picTicketType0.TabStop = false;
            // 
            // lblTicketTypeTPlus
            // 
            lblTicketTypeTPlus.AutoSize = true;
            lblTicketTypeTPlus.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketTypeTPlus.Cursor = Cursors.Hand;
            lblTicketTypeTPlus.Font = new Font("Arial", 24F, FontStyle.Bold);
            lblTicketTypeTPlus.Location = new Point(66, 227);
            lblTicketTypeTPlus.Name = "lblTicketTypeTPlus";
            lblTicketTypeTPlus.Size = new Size(160, 37);
            lblTicketTypeTPlus.TabIndex = 20;
            lblTicketTypeTPlus.Text = "Main title";
            // 
            // lblTicketDescriptionTPlus
            // 
            lblTicketDescriptionTPlus.AutoSize = true;
            lblTicketDescriptionTPlus.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketDescriptionTPlus.Cursor = Cursors.Hand;
            lblTicketDescriptionTPlus.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionTPlus.Location = new Point(69, 267);
            lblTicketDescriptionTPlus.Name = "lblTicketDescriptionTPlus";
            lblTicketDescriptionTPlus.Size = new Size(80, 24);
            lblTicketDescriptionTPlus.TabIndex = 21;
            lblTicketDescriptionTPlus.Text = "Subtitle";
            // 
            // lblTicketDescriptionParisVisite
            // 
            lblTicketDescriptionParisVisite.AutoSize = true;
            lblTicketDescriptionParisVisite.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketDescriptionParisVisite.Cursor = Cursors.Hand;
            lblTicketDescriptionParisVisite.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionParisVisite.Location = new Point(69, 397);
            lblTicketDescriptionParisVisite.Name = "lblTicketDescriptionParisVisite";
            lblTicketDescriptionParisVisite.Size = new Size(80, 24);
            lblTicketDescriptionParisVisite.TabIndex = 23;
            lblTicketDescriptionParisVisite.Text = "Subtitle";
            // 
            // lblTicketTypeParisVisite
            // 
            lblTicketTypeParisVisite.AutoSize = true;
            lblTicketTypeParisVisite.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketTypeParisVisite.Cursor = Cursors.Hand;
            lblTicketTypeParisVisite.Font = new Font("Arial", 24F, FontStyle.Bold);
            lblTicketTypeParisVisite.Location = new Point(66, 357);
            lblTicketTypeParisVisite.Name = "lblTicketTypeParisVisite";
            lblTicketTypeParisVisite.Size = new Size(160, 37);
            lblTicketTypeParisVisite.TabIndex = 22;
            lblTicketTypeParisVisite.Text = "Main title";
            // 
            // lblTicketDescriptionDisneyLand
            // 
            lblTicketDescriptionDisneyLand.AutoSize = true;
            lblTicketDescriptionDisneyLand.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketDescriptionDisneyLand.Cursor = Cursors.Hand;
            lblTicketDescriptionDisneyLand.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionDisneyLand.Location = new Point(69, 526);
            lblTicketDescriptionDisneyLand.Name = "lblTicketDescriptionDisneyLand";
            lblTicketDescriptionDisneyLand.Size = new Size(80, 24);
            lblTicketDescriptionDisneyLand.TabIndex = 25;
            lblTicketDescriptionDisneyLand.Text = "Subtitle";
            // 
            // lblTicketTypeDisney
            // 
            lblTicketTypeDisney.AutoSize = true;
            lblTicketTypeDisney.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketTypeDisney.Cursor = Cursors.Hand;
            lblTicketTypeDisney.Font = new Font("Arial", 24F, FontStyle.Bold);
            lblTicketTypeDisney.Location = new Point(66, 486);
            lblTicketTypeDisney.Name = "lblTicketTypeDisney";
            lblTicketTypeDisney.Size = new Size(160, 37);
            lblTicketTypeDisney.TabIndex = 24;
            lblTicketTypeDisney.Text = "Main title";
            // 
            // lblTicketDescriptionAirport
            // 
            lblTicketDescriptionAirport.AutoSize = true;
            lblTicketDescriptionAirport.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketDescriptionAirport.Cursor = Cursors.Hand;
            lblTicketDescriptionAirport.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionAirport.Location = new Point(69, 655);
            lblTicketDescriptionAirport.Name = "lblTicketDescriptionAirport";
            lblTicketDescriptionAirport.Size = new Size(80, 24);
            lblTicketDescriptionAirport.TabIndex = 27;
            lblTicketDescriptionAirport.Text = "Subtitle";
            // 
            // lblTicketTypeAirport
            // 
            lblTicketTypeAirport.AutoSize = true;
            lblTicketTypeAirport.BackColor = Color.FromArgb(241, 241, 241);
            lblTicketTypeAirport.Cursor = Cursors.Hand;
            lblTicketTypeAirport.Font = new Font("Arial", 24F, FontStyle.Bold);
            lblTicketTypeAirport.Location = new Point(66, 615);
            lblTicketTypeAirport.Name = "lblTicketTypeAirport";
            lblTicketTypeAirport.Size = new Size(160, 37);
            lblTicketTypeAirport.TabIndex = 26;
            lblTicketTypeAirport.Text = "Main title";
            // 
            // TicketMachineTicketSelectionView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(lblTicketDescriptionAirport);
            Controls.Add(lblTicketTypeAirport);
            Controls.Add(lblTicketDescriptionDisneyLand);
            Controls.Add(lblTicketTypeDisney);
            Controls.Add(lblTicketDescriptionParisVisite);
            Controls.Add(lblTicketTypeParisVisite);
            Controls.Add(lblTicketDescriptionTPlus);
            Controls.Add(lblTicketTypeTPlus);
            Controls.Add(picTicketType3);
            Controls.Add(picTicketType2);
            Controls.Add(picTicketType1);
            Controls.Add(picTicketType0);
            Controls.Add(lblTicketSelectionSubtitle);
            Controls.Add(lblTicketSelectionTitle);
            Name = "TicketMachineTicketSelectionView";
            Text = "Ticket Machine - Ticket Selection";
            Controls.SetChildIndex(lblTicketSelectionTitle, 0);
            Controls.SetChildIndex(lblTicketSelectionSubtitle, 0);
            Controls.SetChildIndex(picTicketType0, 0);
            Controls.SetChildIndex(picTicketType1, 0);
            Controls.SetChildIndex(picTicketType2, 0);
            Controls.SetChildIndex(picTicketType3, 0);
            Controls.SetChildIndex(lblTicketTypeTPlus, 0);
            Controls.SetChildIndex(lblTicketDescriptionTPlus, 0);
            Controls.SetChildIndex(lblTicketTypeParisVisite, 0);
            Controls.SetChildIndex(lblTicketDescriptionParisVisite, 0);
            Controls.SetChildIndex(lblTicketTypeDisney, 0);
            Controls.SetChildIndex(lblTicketDescriptionDisneyLand, 0);
            Controls.SetChildIndex(lblTicketTypeAirport, 0);
            Controls.SetChildIndex(lblTicketDescriptionAirport, 0);
            ((System.ComponentModel.ISupportInitialize)picTicketType3).EndInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType2).EndInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType1).EndInit();
            ((System.ComponentModel.ISupportInitialize)picTicketType0).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblTicketSelectionSubtitle;
        private Label lblTicketSelectionTitle;
        private PictureBox picTicketType3;
        private PictureBox picTicketType2;
        private PictureBox picTicketType1;
        private PictureBox picTicketType0;
        private Label lblTicketTypeTPlus;
        private Label lblTicketDescriptionTPlus;
        private Label lblTicketDescriptionParisVisite;
        private Label lblTicketTypeParisVisite;
        private Label lblTicketDescriptionDisneyLand;
        private Label lblTicketTypeDisney;
        private Label lblTicketDescriptionAirport;
        private Label lblTicketTypeAirport;
    }
}