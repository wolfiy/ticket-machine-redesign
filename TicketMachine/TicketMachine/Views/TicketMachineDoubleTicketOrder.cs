﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 27th, 2024

using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// View used for tickets that can be bought for multiple days.
    /// </summary>
    public partial class TicketMachineDoubleTicketOrder 
                       : TicketMachineSimpleTicketOrder
    {
        /// <summary>
        /// Index of the default value to load in the number of days dropdown.
        /// </summary>
        private const int DEFAULT_CBDAYS_SELECTION = 0;

        /// <summary>
        /// Initializes a new <see cref="TicketMachineDoubleTicketOrder"/>.
        /// </summary>
        /// <param name="type">Type of ticket that can be ordered from this 
        /// view.</param>
        public TicketMachineDoubleTicketOrder(TicketType type) : base(type)
        {
            InitializeComponent();
            cbDays.SelectedIndex = DEFAULT_CBDAYS_SELECTION;
            cbDays.SelectedIndexChanged += CbDays_SelectedIndexChanged;
        }

        #region Events
        /// <summary>
        /// Updates the price of the order when the number of days is changed.
        /// </summary>
        /// <param name="sender">The number of days combo box.</param>
        /// <param name="e">A selected index changed event.</param>
        private void CbDays_SelectedIndexChanged(object? sender, EventArgs e)
        {
            if(int.TryParse(cbDays.Text, out int nbDays))
                Controller.SetNbDaysParisVisite(nbDays);
            DisplaySummaryLabels();
        }
        #endregion
    }
}
