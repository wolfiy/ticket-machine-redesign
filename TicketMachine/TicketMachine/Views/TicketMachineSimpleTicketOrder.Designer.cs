﻿namespace TicketMachine.Views
{
    partial class TicketMachineSimpleTicketOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblMainSubtitle = new Label();
            lblMainTitle = new Label();
            lblValidity = new Label();
            lblTicketCategoryStandard = new Label();
            lblTicketDescriptionStandard = new Label();
            lblTicketDescriptionReduced = new Label();
            lblTicketCategoryReduced = new Label();
            lblTicketDescriptionExtra = new Label();
            lblTicketCategoryExtra = new Label();
            btnConfirmPay = new Button();
            btnBuyMore = new Button();
            btnRemoveStandard = new Button();
            btnRemoveReduced = new Button();
            btnRemoveExtra = new Button();
            btnAddExtra = new Button();
            btnAddReduced = new Button();
            btnAddStandard = new Button();
            lblTotalStandard = new Label();
            lblTotalReduced = new Label();
            lblTotalExtra = new Label();
            lblPaymentCart = new Label();
            dateTicket = new DateTimePicker();
            SuspendLayout();
            // 
            // lblMainSubtitle
            // 
            lblMainSubtitle.AutoSize = true;
            lblMainSubtitle.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblMainSubtitle.Location = new Point(48, 161);
            lblMainSubtitle.Name = "lblMainSubtitle";
            lblMainSubtitle.Size = new Size(80, 24);
            lblMainSubtitle.TabIndex = 13;
            lblMainSubtitle.Text = "Subtitle";
            // 
            // lblMainTitle
            // 
            lblMainTitle.AutoSize = true;
            lblMainTitle.Font = new Font("Arial", 38.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblMainTitle.Location = new Point(42, 101);
            lblMainTitle.Name = "lblMainTitle";
            lblMainTitle.Size = new Size(129, 60);
            lblMainTitle.TabIndex = 12;
            lblMainTitle.Text = "Title";
            // 
            // lblValidity
            // 
            lblValidity.AutoSize = true;
            lblValidity.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            lblValidity.Location = new Point(48, 216);
            lblValidity.Name = "lblValidity";
            lblValidity.Size = new Size(94, 24);
            lblValidity.TabIndex = 14;
            lblValidity.Text = "lblValidity";
            // 
            // lblTicketCategoryStandard
            // 
            lblTicketCategoryStandard.AutoSize = true;
            lblTicketCategoryStandard.Font = new Font("Arial", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblTicketCategoryStandard.Location = new Point(48, 278);
            lblTicketCategoryStandard.Name = "lblTicketCategoryStandard";
            lblTicketCategoryStandard.Size = new Size(277, 24);
            lblTicketCategoryStandard.TabIndex = 15;
            lblTicketCategoryStandard.Text = "lblTicketCategoryStandard";
            // 
            // lblTicketDescriptionStandard
            // 
            lblTicketDescriptionStandard.AutoSize = true;
            lblTicketDescriptionStandard.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionStandard.Location = new Point(48, 308);
            lblTicketDescriptionStandard.Name = "lblTicketDescriptionStandard";
            lblTicketDescriptionStandard.Size = new Size(282, 24);
            lblTicketDescriptionStandard.TabIndex = 16;
            lblTicketDescriptionStandard.Text = "lblTicketDescriptionStandard";
            // 
            // lblTicketDescriptionReduced
            // 
            lblTicketDescriptionReduced.AutoSize = true;
            lblTicketDescriptionReduced.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionReduced.Location = new Point(48, 380);
            lblTicketDescriptionReduced.Name = "lblTicketDescriptionReduced";
            lblTicketDescriptionReduced.Size = new Size(282, 24);
            lblTicketDescriptionReduced.TabIndex = 18;
            lblTicketDescriptionReduced.Text = "lblTicketDescriptionReduced";
            // 
            // lblTicketCategoryReduced
            // 
            lblTicketCategoryReduced.AutoSize = true;
            lblTicketCategoryReduced.Font = new Font("Arial", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblTicketCategoryReduced.Location = new Point(48, 350);
            lblTicketCategoryReduced.Name = "lblTicketCategoryReduced";
            lblTicketCategoryReduced.Size = new Size(275, 24);
            lblTicketCategoryReduced.TabIndex = 17;
            lblTicketCategoryReduced.Text = "lblTicketCategoryReduced";
            // 
            // lblTicketDescriptionExtra
            // 
            lblTicketDescriptionExtra.AutoSize = true;
            lblTicketDescriptionExtra.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblTicketDescriptionExtra.Location = new Point(48, 454);
            lblTicketDescriptionExtra.Name = "lblTicketDescriptionExtra";
            lblTicketDescriptionExtra.Size = new Size(245, 24);
            lblTicketDescriptionExtra.TabIndex = 20;
            lblTicketDescriptionExtra.Text = "lblTicketDescriptionExtra";
            // 
            // lblTicketCategoryExtra
            // 
            lblTicketCategoryExtra.AutoSize = true;
            lblTicketCategoryExtra.Font = new Font("Arial", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblTicketCategoryExtra.Location = new Point(48, 424);
            lblTicketCategoryExtra.Name = "lblTicketCategoryExtra";
            lblTicketCategoryExtra.Size = new Size(238, 24);
            lblTicketCategoryExtra.TabIndex = 19;
            lblTicketCategoryExtra.Text = "lblTicketCategoryExtra";
            // 
            // btnConfirmPay
            // 
            btnConfirmPay.AutoSize = true;
            btnConfirmPay.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnConfirmPay.BackColor = Color.FromArgb(0, 196, 180);
            btnConfirmPay.Cursor = Cursors.Hand;
            btnConfirmPay.FlatAppearance.BorderSize = 0;
            btnConfirmPay.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnConfirmPay.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnConfirmPay.FlatStyle = FlatStyle.Flat;
            btnConfirmPay.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnConfirmPay.Location = new Point(48, 576);
            btnConfirmPay.Name = "btnConfirmPay";
            btnConfirmPay.Padding = new Padding(20, 5, 20, 5);
            btnConfirmPay.Size = new Size(197, 44);
            btnConfirmPay.TabIndex = 21;
            btnConfirmPay.Text = "btnConfirmPay";
            btnConfirmPay.UseVisualStyleBackColor = false;
            // 
            // btnBuyMore
            // 
            btnBuyMore.AutoSize = true;
            btnBuyMore.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnBuyMore.BackColor = Color.FromArgb(241, 241, 241);
            btnBuyMore.Cursor = Cursors.Hand;
            btnBuyMore.FlatAppearance.BorderSize = 0;
            btnBuyMore.FlatAppearance.MouseOverBackColor = Color.FromArgb(230, 230, 230);
            btnBuyMore.FlatStyle = FlatStyle.Flat;
            btnBuyMore.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnBuyMore.Location = new Point(48, 636);
            btnBuyMore.Name = "btnBuyMore";
            btnBuyMore.Padding = new Padding(20, 5, 20, 5);
            btnBuyMore.Size = new Size(172, 44);
            btnBuyMore.TabIndex = 22;
            btnBuyMore.Text = "btnBuyMore";
            btnBuyMore.UseVisualStyleBackColor = false;
            btnBuyMore.Click += BtnBuyMore_Click;
            // 
            // btnRemoveStandard
            // 
            btnRemoveStandard.AutoSize = true;
            btnRemoveStandard.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnRemoveStandard.BackColor = Color.FromArgb(0, 196, 180);
            btnRemoveStandard.Cursor = Cursors.Hand;
            btnRemoveStandard.FlatAppearance.BorderSize = 0;
            btnRemoveStandard.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnRemoveStandard.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnRemoveStandard.FlatStyle = FlatStyle.Flat;
            btnRemoveStandard.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnRemoveStandard.Location = new Point(400, 288);
            btnRemoveStandard.Name = "btnRemoveStandard";
            btnRemoveStandard.Padding = new Padding(5, 0, 5, 0);
            btnRemoveStandard.Size = new Size(37, 34);
            btnRemoveStandard.TabIndex = 23;
            btnRemoveStandard.Text = "-";
            btnRemoveStandard.UseVisualStyleBackColor = false;
            // 
            // btnRemoveReduced
            // 
            btnRemoveReduced.AutoSize = true;
            btnRemoveReduced.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnRemoveReduced.BackColor = Color.FromArgb(0, 196, 180);
            btnRemoveReduced.Cursor = Cursors.Hand;
            btnRemoveReduced.FlatAppearance.BorderSize = 0;
            btnRemoveReduced.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnRemoveReduced.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnRemoveReduced.FlatStyle = FlatStyle.Flat;
            btnRemoveReduced.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnRemoveReduced.Location = new Point(400, 360);
            btnRemoveReduced.Name = "btnRemoveReduced";
            btnRemoveReduced.Padding = new Padding(5, 0, 5, 0);
            btnRemoveReduced.Size = new Size(37, 34);
            btnRemoveReduced.TabIndex = 24;
            btnRemoveReduced.Text = "-";
            btnRemoveReduced.UseVisualStyleBackColor = false;
            // 
            // btnRemoveExtra
            // 
            btnRemoveExtra.AutoSize = true;
            btnRemoveExtra.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnRemoveExtra.BackColor = Color.FromArgb(0, 196, 180);
            btnRemoveExtra.Cursor = Cursors.Hand;
            btnRemoveExtra.FlatAppearance.BorderSize = 0;
            btnRemoveExtra.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnRemoveExtra.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnRemoveExtra.FlatStyle = FlatStyle.Flat;
            btnRemoveExtra.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnRemoveExtra.Location = new Point(400, 434);
            btnRemoveExtra.Name = "btnRemoveExtra";
            btnRemoveExtra.Padding = new Padding(5, 0, 5, 0);
            btnRemoveExtra.Size = new Size(37, 34);
            btnRemoveExtra.TabIndex = 25;
            btnRemoveExtra.Text = "-";
            btnRemoveExtra.UseVisualStyleBackColor = false;
            // 
            // btnAddExtra
            // 
            btnAddExtra.AutoSize = true;
            btnAddExtra.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnAddExtra.BackColor = Color.FromArgb(0, 196, 180);
            btnAddExtra.Cursor = Cursors.Hand;
            btnAddExtra.FlatAppearance.BorderSize = 0;
            btnAddExtra.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnAddExtra.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnAddExtra.FlatStyle = FlatStyle.Flat;
            btnAddExtra.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnAddExtra.Location = new Point(480, 434);
            btnAddExtra.Name = "btnAddExtra";
            btnAddExtra.Padding = new Padding(5, 0, 5, 0);
            btnAddExtra.Size = new Size(42, 34);
            btnAddExtra.TabIndex = 28;
            btnAddExtra.Text = "+";
            btnAddExtra.UseVisualStyleBackColor = false;
            // 
            // btnAddReduced
            // 
            btnAddReduced.AutoSize = true;
            btnAddReduced.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnAddReduced.BackColor = Color.FromArgb(0, 196, 180);
            btnAddReduced.Cursor = Cursors.Hand;
            btnAddReduced.FlatAppearance.BorderSize = 0;
            btnAddReduced.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnAddReduced.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnAddReduced.FlatStyle = FlatStyle.Flat;
            btnAddReduced.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnAddReduced.Location = new Point(480, 360);
            btnAddReduced.Name = "btnAddReduced";
            btnAddReduced.Padding = new Padding(5, 0, 5, 0);
            btnAddReduced.Size = new Size(42, 34);
            btnAddReduced.TabIndex = 27;
            btnAddReduced.Text = "+";
            btnAddReduced.UseVisualStyleBackColor = false;
            // 
            // btnAddStandard
            // 
            btnAddStandard.AutoSize = true;
            btnAddStandard.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnAddStandard.BackColor = Color.FromArgb(0, 196, 180);
            btnAddStandard.Cursor = Cursors.Hand;
            btnAddStandard.FlatAppearance.BorderSize = 0;
            btnAddStandard.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnAddStandard.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnAddStandard.FlatStyle = FlatStyle.Flat;
            btnAddStandard.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnAddStandard.Location = new Point(480, 288);
            btnAddStandard.Name = "btnAddStandard";
            btnAddStandard.Padding = new Padding(5, 0, 5, 0);
            btnAddStandard.Size = new Size(42, 34);
            btnAddStandard.TabIndex = 26;
            btnAddStandard.Text = "+";
            btnAddStandard.UseVisualStyleBackColor = false;
            // 
            // lblTotalStandard
            // 
            lblTotalStandard.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            lblTotalStandard.Location = new Point(435, 288);
            lblTotalStandard.Name = "lblTotalStandard";
            lblTotalStandard.Size = new Size(45, 34);
            lblTotalStandard.TabIndex = 29;
            lblTotalStandard.Text = "/";
            lblTotalStandard.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblTotalReduced
            // 
            lblTotalReduced.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            lblTotalReduced.Location = new Point(435, 360);
            lblTotalReduced.Name = "lblTotalReduced";
            lblTotalReduced.Size = new Size(45, 34);
            lblTotalReduced.TabIndex = 30;
            lblTotalReduced.Text = "/";
            lblTotalReduced.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblTotalExtra
            // 
            lblTotalExtra.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            lblTotalExtra.Location = new Point(435, 434);
            lblTotalExtra.Name = "lblTotalExtra";
            lblTotalExtra.Size = new Size(45, 34);
            lblTotalExtra.TabIndex = 31;
            lblTotalExtra.Text = "/";
            lblTotalExtra.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblPaymentCart
            // 
            lblPaymentCart.AutoSize = true;
            lblPaymentCart.Font = new Font("Arial", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblPaymentCart.Location = new Point(590, 216);
            lblPaymentCart.Name = "lblPaymentCart";
            lblPaymentCart.Size = new Size(86, 24);
            lblPaymentCart.TabIndex = 32;
            lblPaymentCart.Text = "Subtitle";
            // 
            // dateTicket
            // 
            dateTicket.Location = new Point(169, 216);
            dateTicket.Name = "dateTicket";
            dateTicket.Size = new Size(200, 23);
            dateTicket.TabIndex = 33;
            // 
            // TicketMachineSimpleTicketOrder
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(dateTicket);
            Controls.Add(lblPaymentCart);
            Controls.Add(lblTotalExtra);
            Controls.Add(lblTotalReduced);
            Controls.Add(lblTotalStandard);
            Controls.Add(btnAddExtra);
            Controls.Add(btnAddReduced);
            Controls.Add(btnAddStandard);
            Controls.Add(btnRemoveExtra);
            Controls.Add(btnRemoveReduced);
            Controls.Add(btnRemoveStandard);
            Controls.Add(btnBuyMore);
            Controls.Add(btnConfirmPay);
            Controls.Add(lblTicketDescriptionExtra);
            Controls.Add(lblTicketCategoryExtra);
            Controls.Add(lblTicketDescriptionReduced);
            Controls.Add(lblTicketCategoryReduced);
            Controls.Add(lblTicketDescriptionStandard);
            Controls.Add(lblTicketCategoryStandard);
            Controls.Add(lblValidity);
            Controls.Add(lblMainSubtitle);
            Controls.Add(lblMainTitle);
            Name = "TicketMachineSimpleTicketOrder";
            Text = "Ticket Machine - Simple Ticket";
            Controls.SetChildIndex(lblMainTitle, 0);
            Controls.SetChildIndex(lblMainSubtitle, 0);
            Controls.SetChildIndex(lblValidity, 0);
            Controls.SetChildIndex(lblTicketCategoryStandard, 0);
            Controls.SetChildIndex(lblTicketDescriptionStandard, 0);
            Controls.SetChildIndex(lblTicketCategoryReduced, 0);
            Controls.SetChildIndex(lblTicketDescriptionReduced, 0);
            Controls.SetChildIndex(lblTicketCategoryExtra, 0);
            Controls.SetChildIndex(lblTicketDescriptionExtra, 0);
            Controls.SetChildIndex(btnConfirmPay, 0);
            Controls.SetChildIndex(btnBuyMore, 0);
            Controls.SetChildIndex(btnRemoveStandard, 0);
            Controls.SetChildIndex(btnRemoveReduced, 0);
            Controls.SetChildIndex(btnRemoveExtra, 0);
            Controls.SetChildIndex(btnAddStandard, 0);
            Controls.SetChildIndex(btnAddReduced, 0);
            Controls.SetChildIndex(btnAddExtra, 0);
            Controls.SetChildIndex(lblTotalStandard, 0);
            Controls.SetChildIndex(lblTotalReduced, 0);
            Controls.SetChildIndex(lblTotalExtra, 0);
            Controls.SetChildIndex(lblPaymentCart, 0);
            Controls.SetChildIndex(dateTicket, 0);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblMainSubtitle;
        private Label lblMainTitle;
        private Label lblValidity;
        private Label lblTicketCategoryStandard;
        private Label lblTicketDescriptionStandard;
        private Label lblTicketDescriptionReduced;
        private Label lblTicketCategoryReduced;
        private Label lblTicketDescriptionExtra;
        private Label lblTicketCategoryExtra;
        private Button btnConfirmPay;
        private Button btnBuyMore;
        private Button btnRemoveStandard;
        private Button btnRemoveReduced;
        private Button btnRemoveExtra;
        private Button btnAddExtra;
        private Button btnAddReduced;
        private Button btnAddStandard;
        private Label lblTotalStandard;
        private Label lblTotalReduced;
        private Label lblTotalExtra;
        private Label lblPaymentCart;
        private DateTimePicker dateTicket;
    }
}