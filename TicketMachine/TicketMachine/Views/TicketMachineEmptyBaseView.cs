/// ETML
/// Author: Sebastien TILLE
/// Date: May 2nd, 2024

using System.Resources;
using TicketMachine.Controllers;
using TicketMachine.Utils;
using TicketMachine.Views;

namespace TicketMachine
{
    /// <summary>
    /// This class serves as a basis for all other views. It is capable of
    /// handling different languages and has a home button that does not cancel
    /// the current order (useful for debugging).
    /// </summary>
    public partial class TicketMachineEmptyBaseView : Form
    {
        /// <summary>
        /// Handles user interactions with the view.
        /// </summary>
        public TicketMachineController Controller { get; set; }

        /// <summary>
        /// Initializes a new <see cref="TicketMachineEmptyBaseView"/>.
        /// </summary>
        /// This class should not be instantiated, but the abstract keyword 
        /// breaks the Designer. Marked protected instead.
        /// </remarks>
        protected TicketMachineEmptyBaseView()
        {
            InitializeComponent();

            this.Icon = Resources.Assets.icon;
            this.picGerman.Click += Lang_Click;
            this.picEnglish.Click += Lang_Click;
            this.picFrench.Click += Lang_Click;
            this.picItalian.Click += Lang_Click;
            this.picSpanish.Click += Lang_Click;
            this.picHome.Click += Home_Click;
            this.FormClosing += Form_Closing;
        }

        #region Language
        /// <summary>
        /// Updates the language of the view using the provided resource 
        /// manager.
        /// </summary>
        /// <param name="resourceManager">An instance of 
        /// <see cref="ResourceManager"/> that contains all string required by 
        /// this view.</param>
        public virtual void UpdateLanguage(ResourceManager resourceManager)
        {
            foreach (Control c in Controls)
            {
                if (resourceManager.GetString(c.Name) != null)
                    c.Text = resourceManager.GetString(c.Name);
            }
        }

        /// <summary>
        /// Changes the language of the program based on the clicked "button".
        /// </summary>
        /// <param name="sender">The clicked language "button".</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the seender is not a 
        /// language "button" (picture box).</exception>
        private void Lang_Click(object? sender, EventArgs e)
        {
            if (sender is not PictureBox langSelection)
                throw new ArgumentException("Sender is invalid!", 
                                            nameof(sender));

            Lang lang = langSelection.Name switch
            {
                nameof(picGerman) => Lang.DE,
                nameof(picEnglish) => Lang.EN,
                nameof(picFrench) => Lang.FR,
                nameof(picItalian) => Lang.IT,
                nameof(picSpanish) => Lang.ES,
                _ => throw new ArgumentException("Unknown language.",
                                                 nameof(langSelection))
            };

            Controller.SetLang(lang);
        }
        #endregion

        #region Locale
        /// <summary>
        /// Updates the header with the current date and time.
        /// </summary>
        public void UpdateDateTime()
        {
            lblTime.Text = Controller.GetTime();
            lblDate.Text = Controller.GetDate();
        }
        #endregion

        #region Window management
        /// <summary>
        /// Displays the form.
        /// </summary>
        public virtual void Open()
        {
            this.Show();
        }

        /// <summary>
        /// Hides the form.
        /// </summary>
        public new void Close()
        {
            this.Hide();
        }

        /// <summary>
        /// Goes back to the home view.
        /// </summary>
        /// <param name="sender">The home button (RATP logo).</param>
        /// <param name="e">A click event.</param>
        private void Home_Click(object? sender, EventArgs e)
        {
            // Do not open the home view if already on the home view
            if (this is TicketMachineHomeView) 
                return;
            
            Controller.OpenView(this, ViewType.Home);
        }

        /// <summary>
        /// Quits the application when the form is closing.
        /// </summary>
        /// <param name="sender">This form.</param>
        /// <param name="e">A form closing event.</param>
        private void Form_Closing(object? sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion
    }
}
