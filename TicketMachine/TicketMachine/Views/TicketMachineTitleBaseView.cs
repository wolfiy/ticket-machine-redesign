﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 13th, 2024

namespace TicketMachine.Views
{
    /// <summary>
    /// This class serves as a basis for views which have a title and a subtitle
    /// on the top of the screen, such as the ticket selection or payment views.
    /// </summary>
    public partial class TicketMachineTitleBaseView : TicketMachineEmptyBaseView
    {
        /// <summary>
        /// Initializes a new <see cref="TicketMachineTitleBaseView"/>.
        /// </summary>
        /// <remarks>
        /// This class should not be instantiated, but the abstract keyword 
        /// breaks the Designer. Marked protected instead.
        /// </remarks>
        protected TicketMachineTitleBaseView() : base()
        {
            InitializeComponent();
        }
    }
}
