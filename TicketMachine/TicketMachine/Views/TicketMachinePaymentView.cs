﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 23rd, 2024

using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// View prompting the user to pay either by cash or card. It displays
    /// all available payment methods.
    /// </summary>
    public partial class TicketMachinePaymentView : TicketMachineBaseWithSummary
    {
        /// <summary>
        /// Default title name of this view.
        /// </summary>
        private const string DEFAULT_TITLE_NAME = "lblPaymentTitle";

        /// <summary>
        /// Summary location on the view.
        /// </summary>
        private readonly Point SUMMARY_LOCATION = new(48, 245);

        /// <summary>
        /// Original title.
        /// </summary>
        private readonly string _originalTitleName;

        /// <summary>
        /// Original subtitle.
        /// </summary>
        private readonly string _originalSubtitleName;

        /// <summary>
        /// Initializes a new <see cref="TicketMachinePaymentView"/>.
        /// </summary>
        public TicketMachinePaymentView()
        {
            InitializeComponent();
            _summaryLocation = SUMMARY_LOCATION;

            _originalTitleName = lblPaymentTitle.Name;
            _originalSubtitleName = lblPaymentSubtitle.Name;

            btnBack.Click += BtnBack_Click;
            btnCash.Click += BtnCash_Clik;
            btnCard.Click += BtnCard_Clik;
            btnCancelOrder.Click += BtnCancelOrder_Click;
        }

        #region GUI
        /// <summary>
        /// Resets the label name and text to the original values.
        /// </summary>
        private void ResetGUIText()
        {
            lblPaymentTitle.Name = _originalTitleName;
            lblPaymentSubtitle.Name = _originalSubtitleName;
            Controller.SetLang(Controller.Lang);
        }
        #endregion

        #region Events
        /// <summary>
        /// Opens the ticket selection when the back button is clicked.
        /// </summary>
        /// <param name="sender">The back button.</param>
        /// <param name="e">A click event.</param>
        private void BtnBack_Click(object? sender, EventArgs e)
        {
            Controller.OpenView(this, ViewType.Selection);
            ResetGUIText();
        }

        /// <summary>
        /// Cancels the current order and clears the cart.
        /// </summary>
        /// <param name="sender">The cancel order button.</param>
        /// <param name="e">A click event.</param>
        private void BtnCancelOrder_Click(object? sender, EventArgs e)
        {
            if (MessageBox.Show(Controller.GetString("cancelOrderConfirm"),
                                Controller.GetString("btnCancelOrder"),
                                MessageBoxButtons.YesNo)
                == DialogResult.No)
                return;

            Controller.ClearCart();
            Controller.OpenView(this, ViewType.Home);
            ResetGUIText();
        }

        /// <summary>
        /// Prompts the user to insert cash.
        /// </summary>
        /// <param name="sender">The 'pay by cash' button.</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the sender is not a button.
        /// </exception>
        private void BtnCash_Clik(object? sender, EventArgs e)
        {
            if (sender is not Button button)
                throw new ArgumentException("Invalid sender!",
                                            nameof(sender));

            lblPaymentTitle.Name = "lblPaymentTitleCash";
            lblPaymentSubtitle.Name = "lblPaymentSubtitleTerminal";
            Controller.SetLang(Controller.Lang);
        }

        /// <summary>
        /// Prompts the user to use their card.
        /// </summary>
        /// <param name="sender">The 'pay by card' button.</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the sender is not a button.
        /// </exception>
        private void BtnCard_Clik(object? sender, EventArgs e)
        {
            if (sender is not Button button)
                throw new ArgumentException("Invalid sender!",
                                            nameof(sender));

            lblPaymentTitle.Name = "lblPaymentTitleCard";
            lblPaymentSubtitle.Name = "lblPaymentSubtitleTerminal";
            Controller.SetLang(Controller.Lang);
        }
        #endregion
    }
}
