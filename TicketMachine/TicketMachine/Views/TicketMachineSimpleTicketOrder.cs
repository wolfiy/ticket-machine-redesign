﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 16th, 2024

using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// Ticket purchase view of the ticket machine program.
    /// </summary>
    public partial class TicketMachineSimpleTicketOrder 
                       : TicketMachineBaseWithSummary
    {
        /// <summary>
        /// Type of ticket that can be purchased from this view.
        /// </summary>
        protected readonly TicketType _type;

        /// <summary>
        /// Initializes a new <see cref="TicketMachineSimpleTicketOrder"/>.
        /// </summary>
        /// <remarks>
        /// This contructor is required by the designer to properly display
        /// children classes.
        /// </remarks>
        private TicketMachineSimpleTicketOrder()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new <see cref="TicketMachineSimpleTicketOrder"/>.
        /// </summary>
        /// <param name="type">Type of ticket to order.</param>
        public TicketMachineSimpleTicketOrder(TicketType type) : this()
        {
            InitializeComponent();
            _activeSummarylabels = [];
            _type = type;

            // Adjusts the title depending on the type of ticket this view is
            // used for
            lblMainTitle.Name = type switch
            {
                TicketType.TPlus => "lblTicketTypeTPlus",
                TicketType.ParisVisite => "lblTicketTypeParisVisite",
                TicketType.Disneyland => "lblTicketTypeDisney",
                TicketType.Airport => "lblTicketTypeAirport",
                _ => throw new ArgumentException("Unknown label.", 
                                                 nameof(type))
            };

            // Adjusts the subtitle depending on the type of ticket this view
            // is used for
            lblMainSubtitle.Name = type switch
            {
                TicketType.TPlus => "lblTicketDescriptionTPlus",
                TicketType.ParisVisite => "lblTicketDescriptionParisVisite",
                TicketType.Disneyland => "lblTicketDescriptionDisneyLand",
                TicketType.Airport => "lblTicketDescriptionAirport",
                _ => throw new ArgumentException("Unknown label", 
                                                 nameof(type))
            };

            btnAddStandard.Click += BtnAdd_Click;
            btnAddReduced.Click += BtnAdd_Click;
            btnAddExtra.Click += BtnAdd_Click;

            btnRemoveStandard.Click += BtnRemove_Click;
            btnRemoveReduced.Click += BtnRemove_Click;
            btnRemoveExtra.Click += BtnRemove_Click;

            btnConfirmPay.Click += BtnPay_Click;
        }

        #region Window management
        /// <summary>
        /// Opens the view and refreshes the current tickets counts.
        /// </summary>
        public override void Open()
        {
            base.Open();
            UpdateCount();
        }
        #endregion

        #region Ticket count and order summary
        /// <summary>
        /// Refreshes the summary and updates the number of tickets currently
        /// in the cart.
        /// </summary>
        private void UpdateCount()
        {
            UpdateTicketCount(PriceTier.Standard, lblTotalStandard);
            UpdateTicketCount(PriceTier.Reduced, lblTotalReduced);
            UpdateTicketCount(PriceTier.Extra, lblTotalExtra);

            UpdateSummary();
        }

        /// <summary>
        /// Refreshes the number of tickets of a given tier in the cart.
        /// </summary>
        /// <param name="tier">Price tier of the ticket.</param>
        /// <param name="label">Associated label.</param>
        private void UpdateTicketCount(PriceTier tier, Label label)
        {
            label.Text = Controller.GetTicketCount(_type, tier).ToString();
        }
        #endregion

        #region Events
        /// <summary>
        /// Adds a ticket to the cart.
        /// </summary>
        /// <param name="sender">An '+' (add) button.</param>
        /// <param name="e">A click event.</param>
        private void BtnAdd_Click(object? sender, EventArgs e)
        {
            Button b = (Button)sender;

            _ = b.Name switch
            {
                "btnAddStandard" => Controller.AddTicket(_type, PriceTier.Standard, dateTicket.Value),
                "btnAddReduced" => Controller.AddTicket(_type, PriceTier.Reduced, dateTicket.Value),
                "btnAddExtra" => Controller.AddTicket(_type, PriceTier.Extra, dateTicket.Value),
                _ => throw new ArgumentException()
            };

            UpdateCount();
        }

        /// <summary>
        /// Removes a ticket from the cart.
        /// </summary>
        /// <param name="sender">A '-' (remove) button.</param>
        /// <param name="e">A click event.</param>
        private void BtnRemove_Click(object? sender, EventArgs e) 
        { 
            Button b = (Button)sender;

            _ = b.Name switch
            {
                "btnRemoveStandard" => Controller.RemoveTicket(_type, PriceTier.Standard),
                "btnRemoveReduced" => Controller.RemoveTicket(_type, PriceTier.Reduced),
                "btnRemoveExtra" => Controller.RemoveTicket(_type, PriceTier.Extra),
                _ => throw new ArgumentException()
            };

            UpdateCount();
        }

        /// <summary>
        /// Opens the payment view.
        /// </summary>
        /// <param name="sender">The 'Confirm and pay' button.</param>
        /// <param name="e">A click event.</param>
        private void BtnPay_Click(object? sender, EventArgs e)
        {
            Controller.OpenView(this, ViewType.Payment);
        }

        /// <summary>
        /// Re-open the ticket selection view.
        /// </summary>
        /// <param name="sender">The buy more button.</param>
        /// <param name="e">A click event.</param>
        private void BtnBuyMore_Click(object sender, EventArgs e)
        {
            Controller.OpenView(this, Utils.ViewType.Selection);
        }
        #endregion
    }
}
