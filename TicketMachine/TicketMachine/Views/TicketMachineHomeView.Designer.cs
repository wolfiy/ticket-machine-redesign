﻿namespace TicketMachine.Views
{
    partial class TicketMachineHomeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblHomeSubtitle = new Label();
            lblHomeTitle = new Label();
            picNavigo = new PictureBox();
            picTicket = new PictureBox();
            lblHomeNavigo = new Label();
            lblHomeTicket = new Label();
            ((System.ComponentModel.ISupportInitialize)picNavigo).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picTicket).BeginInit();
            SuspendLayout();
            // 
            // lblHomeSubtitle
            // 
            lblHomeSubtitle.AutoSize = true;
            lblHomeSubtitle.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblHomeSubtitle.Location = new Point(48, 161);
            lblHomeSubtitle.Name = "lblHomeSubtitle";
            lblHomeSubtitle.Size = new Size(80, 24);
            lblHomeSubtitle.TabIndex = 13;
            lblHomeSubtitle.Text = "Subtitle";
            // 
            // lblHomeTitle
            // 
            lblHomeTitle.AutoSize = true;
            lblHomeTitle.Font = new Font("Arial", 38.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblHomeTitle.Location = new Point(42, 101);
            lblHomeTitle.Name = "lblHomeTitle";
            lblHomeTitle.Size = new Size(244, 60);
            lblHomeTitle.TabIndex = 12;
            lblHomeTitle.Text = "Main title";
            // 
            // picNavigo
            // 
            picNavigo.Cursor = Cursors.Hand;
            picNavigo.Image = Resources.Assets.Navigo;
            picNavigo.Location = new Point(51, 223);
            picNavigo.Name = "picNavigo";
            picNavigo.Size = new Size(423, 457);
            picNavigo.SizeMode = PictureBoxSizeMode.AutoSize;
            picNavigo.TabIndex = 14;
            picNavigo.TabStop = false;
            // 
            // picTicket
            // 
            picTicket.Cursor = Cursors.Hand;
            picTicket.Image = Resources.Assets.Ticket;
            picTicket.Location = new Point(539, 223);
            picTicket.Name = "picTicket";
            picTicket.Size = new Size(423, 457);
            picTicket.SizeMode = PictureBoxSizeMode.AutoSize;
            picTicket.TabIndex = 15;
            picTicket.TabStop = false;
            // 
            // lblHomeNavigo
            // 
            lblHomeNavigo.BackColor = Color.FromArgb(241, 241, 241);
            lblHomeNavigo.Cursor = Cursors.Hand;
            lblHomeNavigo.Font = new Font("Arial", 32F, FontStyle.Bold);
            lblHomeNavigo.Location = new Point(51, 348);
            lblHomeNavigo.Name = "lblHomeNavigo";
            lblHomeNavigo.Size = new Size(423, 60);
            lblHomeNavigo.TabIndex = 17;
            lblHomeNavigo.Text = "Main title";
            lblHomeNavigo.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblHomeTicket
            // 
            lblHomeTicket.BackColor = Color.FromArgb(241, 241, 241);
            lblHomeTicket.Cursor = Cursors.Hand;
            lblHomeTicket.Font = new Font("Arial", 32F, FontStyle.Bold);
            lblHomeTicket.Location = new Point(539, 348);
            lblHomeTicket.Name = "lblHomeTicket";
            lblHomeTicket.Size = new Size(423, 60);
            lblHomeTicket.TabIndex = 18;
            lblHomeTicket.Text = "Main title";
            lblHomeTicket.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // TicketMachineHomeView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(lblHomeTicket);
            Controls.Add(lblHomeNavigo);
            Controls.Add(picTicket);
            Controls.Add(picNavigo);
            Controls.Add(lblHomeSubtitle);
            Controls.Add(lblHomeTitle);
            Name = "TicketMachineHomeView";
            Text = "Ticket Machine - Home";
            Controls.SetChildIndex(lblHomeTitle, 0);
            Controls.SetChildIndex(lblHomeSubtitle, 0);
            Controls.SetChildIndex(picNavigo, 0);
            Controls.SetChildIndex(picTicket, 0);
            Controls.SetChildIndex(lblHomeNavigo, 0);
            Controls.SetChildIndex(lblHomeTicket, 0);
            ((System.ComponentModel.ISupportInitialize)picNavigo).EndInit();
            ((System.ComponentModel.ISupportInitialize)picTicket).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblHomeSubtitle;
        private Label lblHomeTitle;
        private PictureBox picNavigo;
        private PictureBox picTicket;
        private Label lblHomeNavigo;
        private Label lblHomeTicket;
    }
}