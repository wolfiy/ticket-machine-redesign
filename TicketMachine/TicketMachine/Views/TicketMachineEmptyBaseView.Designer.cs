﻿namespace TicketMachine
{
    partial class TicketMachineEmptyBaseView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            picFrench = new PictureBox();
            picEnglish = new PictureBox();
            picGerman = new PictureBox();
            picSpanish = new PictureBox();
            picItalian = new PictureBox();
            lblDate = new Label();
            lblTime = new Label();
            picHome = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)picFrench).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picEnglish).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picGerman).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picSpanish).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picItalian).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picHome).BeginInit();
            SuspendLayout();
            // 
            // picFrench
            // 
            picFrench.Cursor = Cursors.Hand;
            picFrench.Image = Resources.Assets.france;
            picFrench.Location = new Point(733, 22);
            picFrench.Name = "picFrench";
            picFrench.Size = new Size(45, 30);
            picFrench.SizeMode = PictureBoxSizeMode.StretchImage;
            picFrench.TabIndex = 0;
            picFrench.TabStop = false;
            // 
            // picEnglish
            // 
            picEnglish.Cursor = Cursors.Hand;
            picEnglish.Image = Resources.Assets.uk;
            picEnglish.Location = new Point(784, 22);
            picEnglish.Name = "picEnglish";
            picEnglish.Size = new Size(45, 30);
            picEnglish.SizeMode = PictureBoxSizeMode.StretchImage;
            picEnglish.TabIndex = 1;
            picEnglish.TabStop = false;
            // 
            // picGerman
            // 
            picGerman.Cursor = Cursors.Hand;
            picGerman.Image = Resources.Assets.germany;
            picGerman.Location = new Point(886, 22);
            picGerman.Name = "picGerman";
            picGerman.Size = new Size(45, 30);
            picGerman.SizeMode = PictureBoxSizeMode.StretchImage;
            picGerman.TabIndex = 2;
            picGerman.TabStop = false;
            // 
            // picSpanish
            // 
            picSpanish.Cursor = Cursors.Hand;
            picSpanish.Image = Resources.Assets.spain;
            picSpanish.Location = new Point(835, 22);
            picSpanish.Name = "picSpanish";
            picSpanish.Size = new Size(45, 30);
            picSpanish.SizeMode = PictureBoxSizeMode.StretchImage;
            picSpanish.TabIndex = 3;
            picSpanish.TabStop = false;
            // 
            // picItalian
            // 
            picItalian.Cursor = Cursors.Hand;
            picItalian.Image = Resources.Assets.italia;
            picItalian.Location = new Point(937, 22);
            picItalian.Name = "picItalian";
            picItalian.Size = new Size(45, 30);
            picItalian.SizeMode = PictureBoxSizeMode.StretchImage;
            picItalian.TabIndex = 4;
            picItalian.TabStop = false;
            // 
            // lblDate
            // 
            lblDate.Font = new Font("Arial", 19F);
            lblDate.Location = new Point(282, 22);
            lblDate.Name = "lblDate";
            lblDate.Size = new Size(425, 31);
            lblDate.TabIndex = 5;
            lblDate.Text = "dd mmmm yyyy";
            lblDate.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblTime
            // 
            lblTime.AutoSize = true;
            lblTime.Font = new Font("Arial", 19F);
            lblTime.Location = new Point(171, 22);
            lblTime.Name = "lblTime";
            lblTime.Size = new Size(93, 31);
            lblTime.TabIndex = 6;
            lblTime.Text = "hh:mm";
            // 
            // picHome
            // 
            picHome.Image = Resources.Assets.logo;
            picHome.Location = new Point(25, 15);
            picHome.Name = "picHome";
            picHome.Size = new Size(45, 45);
            picHome.SizeMode = PictureBoxSizeMode.StretchImage;
            picHome.TabIndex = 7;
            picHome.TabStop = false;
            // 
            // TicketMachineEmptyBaseView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.White;
            ClientSize = new Size(1008, 729);
            Controls.Add(picHome);
            Controls.Add(lblTime);
            Controls.Add(lblDate);
            Controls.Add(picItalian);
            Controls.Add(picSpanish);
            Controls.Add(picGerman);
            Controls.Add(picEnglish);
            Controls.Add(picFrench);
            MaximizeBox = false;
            MaximumSize = new Size(1024, 768);
            MinimumSize = new Size(1024, 768);
            Name = "TicketMachineEmptyBaseView";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "TicketMachine - Empty Base";
            ((System.ComponentModel.ISupportInitialize)picFrench).EndInit();
            ((System.ComponentModel.ISupportInitialize)picEnglish).EndInit();
            ((System.ComponentModel.ISupportInitialize)picGerman).EndInit();
            ((System.ComponentModel.ISupportInitialize)picSpanish).EndInit();
            ((System.ComponentModel.ISupportInitialize)picItalian).EndInit();
            ((System.ComponentModel.ISupportInitialize)picHome).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private PictureBox picFrench;
        private PictureBox picEnglish;
        private PictureBox picGerman;
        private PictureBox picSpanish;
        private PictureBox picItalian;
        private Label lblDate;
        private Label lblTime;
        private PictureBox picHome;
    }
}
