﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 28th, 2024

using System.Resources;
using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// A base for views that need to display a summary of the current order.
    /// </summary>
    public partial class TicketMachineBaseWithSummary 
                       : TicketMachineEmptyBaseView
    {
        /// <summary>
        /// The minimum number of tickets of a certain combination that need
        /// to be in the cart for the associated label to be displayed in the
        /// summary.
        /// </summary>
        protected const int MIN_TICKETS_IN_CART_FOR_SUMMARY = 1;

        /// <summary>
        /// Default number of days.
        /// </summary>
        private const int DEFAULT_NB_DAYS = 1;

        /// <summary>
        /// Space between each lines of the summary.
        /// </summary>
        private const int SUMMARY_SPACING = 20;

        /// <summary>
        /// Offset of the first summary element other than 'Summary' text 
        /// itself.
        /// </summary>
        private const int SUMMARY_OFFSET = 15;

        /// <summary>
        /// Size of summary labels.
        /// </summary>
        private const float SUMMARY_FONT_SIZE = 15.75f;

        /// <summary>
        /// Font to use in summary labels.
        /// </summary>
        private const string SUMMARY_FONT_FACE = "Arial";

        /// <summary>
        /// Default position of the 'Summary' text.
        /// </summary>
        protected Point _summaryLocation = new(590, 252);

        /// <summary>
        /// Labels currently displayed in the order summary.
        /// </summary >
        protected List<Label> _activeSummarylabels = [];

        /// <summary>
        /// Initializes a new <see cref="TicketMachineBaseWithSummary"/>.
        /// This constructor is only used by the Windows Form Designer.
        /// </summary>
        /// </summary>
        /// This class should not be instantiated, but the abstract keyword 
        /// breaks the Designer. Marked protected instead.
        /// </remarks>
        protected TicketMachineBaseWithSummary()
        {
            InitializeComponent();
            _activeSummarylabels = [];
        }

        #region Window management
        /// <summary>
        /// Opens the view and refreshes the summary.
        /// </summary>
        public override void Open()
        {
            base.Open();
            UpdateSummary();
        }
        #endregion

        #region Language
        /// <inheritdoc/>
        public override void UpdateLanguage(ResourceManager resourceManager)
        {
            base.UpdateLanguage(resourceManager);
            DisplaySummaryLabels();
        }
        #endregion

        #region Summary
        /// <summary>
        /// Refreshes the summary.
        /// </summary>
        protected void UpdateSummary()
        {
            CreateSummaryLabels();
            DisplaySummaryLabels();
        }

        /// <summary>
        /// Displays a summary of the current order.
        /// </summary>
        protected void DisplaySummaryLabels()
        {
            // If a label was already present, remove it
            foreach (Label l in _activeSummarylabels)
                Controls.Remove(l);

            // Reset active labels
            _activeSummarylabels.Clear();

            // Add newly created labels
            foreach (Label l in CreateSummaryLabels())
            {
                Controls.Add(l);
                _activeSummarylabels.Add(l);
            }
        }

        /// <summary>
        /// Creates labels needed to display a summary of the current order.
        /// </summary>
        protected List<Label> CreateSummaryLabels()
        {
            // Active labels counter used for positioning
            int i = 0;
            int nbDays = DEFAULT_NB_DAYS;
            double total = 0;
            List<Label> labels = [];

            // Check all ticket combinations
            foreach (var (type, tier) in Controller.GetTicketCombinations())
            {
                // Get detailed info
                var ticket = Controller.GetTicketDetails(type, tier);

                // Skip combination if none have been added to cart
                if (ticket.count < MIN_TICKETS_IN_CART_FOR_SUMMARY)
                    continue;

                // Get number of days
                if (type is TicketType.ParisVisite)
                    nbDays = Controller.GetNbDaysParisVisite();
                else
                    nbDays = DEFAULT_NB_DAYS;

                // Add price to total
                total += ticket.count * ticket.price * nbDays;

                // Create new label for ticket combination
                Label label = new()
                {
                    AutoSize = true,
                    Font = new Font(SUMMARY_FONT_FACE, SUMMARY_FONT_SIZE),
                    Location = new Point(_summaryLocation.X,
                                         _summaryLocation.Y
                                         + i * SUMMARY_SPACING),
                    Text = $"{ticket.count}x {ticket.name}, " +
                           $"{ticket.count * ticket.price * nbDays}€ ✏️",
                    Cursor = Cursors.Hand
                };

                // Add click event
                label.Click += (sender, e) =>
                {
                    Controller.OpenView(ticket.type);
                };

                // Add label to list and increment counter
                labels.Add(label);
                ++i;
            }

            // Total label
            Label labelTotal = new()
            {
                AutoSize = true,
                Font = new Font(SUMMARY_FONT_FACE, 
                                SUMMARY_FONT_SIZE, 
                                FontStyle.Bold),
                Location = new Point(_summaryLocation.X,
                                     _summaryLocation.Y + i * SUMMARY_SPACING 
                                                        + SUMMARY_OFFSET),
                Text = $"TOTAL: {total}€"
            };

            labels.Add(labelTotal);

            return labels;
        }
        #endregion
    }
}
