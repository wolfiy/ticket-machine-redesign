﻿namespace TicketMachine.Views
{
    partial class TicketMachineTitleBaseView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblMainSubtitle = new Label();
            lblMainTitle = new Label();
            SuspendLayout();
            // 
            // lblMainSubtitle
            // 
            lblMainSubtitle.AutoSize = true;
            lblMainSubtitle.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblMainSubtitle.Location = new Point(48, 161);
            lblMainSubtitle.Name = "lblMainSubtitle";
            lblMainSubtitle.Size = new Size(80, 24);
            lblMainSubtitle.TabIndex = 11;
            lblMainSubtitle.Text = "Subtitle";
            // 
            // lblMainTitle
            // 
            lblMainTitle.AutoSize = true;
            lblMainTitle.Font = new Font("Arial", 38.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblMainTitle.Location = new Point(42, 101);
            lblMainTitle.Name = "lblMainTitle";
            lblMainTitle.Size = new Size(244, 60);
            lblMainTitle.TabIndex = 10;
            lblMainTitle.Text = "Main title";
            // 
            // TicketMachineTitleBaseView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(lblMainSubtitle);
            Controls.Add(lblMainTitle);
            Name = "TicketMachineTitleBaseView";
            Text = "Ticket Machine - Title Base";
            Controls.SetChildIndex(lblMainTitle, 0);
            Controls.SetChildIndex(lblMainSubtitle, 0);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblMainSubtitle;
        private Label lblMainTitle;
    }
}