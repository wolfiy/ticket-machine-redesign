﻿namespace TicketMachine.Views
{
    partial class TicketMachineDoubleTicketOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblTicketCategoryDays = new Label();
            lblTicketDescriptionDays = new Label();
            cbDays = new ComboBox();
            SuspendLayout();
            // 
            // lblTicketCategoryDays
            // 
            lblTicketCategoryDays.AutoSize = true;
            lblTicketCategoryDays.Font = new Font("Arial", 15.75F, FontStyle.Bold);
            lblTicketCategoryDays.Location = new Point(48, 498);
            lblTicketCategoryDays.Name = "lblTicketCategoryDays";
            lblTicketCategoryDays.Size = new Size(235, 24);
            lblTicketCategoryDays.TabIndex = 34;
            lblTicketCategoryDays.Text = "lblTicketCategoryDays";
            // 
            // lblTicketDescriptionDays
            // 
            lblTicketDescriptionDays.AutoSize = true;
            lblTicketDescriptionDays.Font = new Font("Arial", 15.75F, FontStyle.Italic);
            lblTicketDescriptionDays.Location = new Point(49, 522);
            lblTicketDescriptionDays.Name = "lblTicketDescriptionDays";
            lblTicketDescriptionDays.Size = new Size(245, 24);
            lblTicketDescriptionDays.TabIndex = 35;
            lblTicketDescriptionDays.Text = "lblTicketDescriptionDays";
            // 
            // cbDays
            // 
            cbDays.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDays.Font = new Font("Arial", 15.75F);
            cbDays.FormattingEnabled = true;
            cbDays.Items.AddRange(new object[] { "1", "3", "5" });
            cbDays.Location = new Point(400, 509);
            cbDays.Name = "cbDays";
            cbDays.Size = new Size(122, 32);
            cbDays.TabIndex = 36;
            // 
            // TicketMachineDoubleTicketOrder
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(cbDays);
            Controls.Add(lblTicketDescriptionDays);
            Controls.Add(lblTicketCategoryDays);
            Name = "TicketMachineDoubleTicketOrder";
            Text = "TicketMachineDoubleTicketOrder";
            Controls.SetChildIndex(lblTicketCategoryDays, 0);
            Controls.SetChildIndex(lblTicketDescriptionDays, 0);
            Controls.SetChildIndex(cbDays, 0);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblTicketCategoryDays;
        private Label lblTicketDescriptionDays;
        private ComboBox cbDays;
    }
}