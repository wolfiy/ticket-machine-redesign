﻿namespace TicketMachine.Views
{
    partial class TicketMachinePaymentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblPaymentSubtitle = new Label();
            lblPaymentTitle = new Label();
            lblPaymentCart = new Label();
            btnBack = new Button();
            lblPaymentCards = new Label();
            lblPaymentCoins = new Label();
            picVisa = new PictureBox();
            pic2Eur = new PictureBox();
            picUnionPay = new PictureBox();
            picApplePay = new PictureBox();
            picGooglePay = new PictureBox();
            picMastercard = new PictureBox();
            pic1Eur = new PictureBox();
            pic050Eur = new PictureBox();
            pic020Eur = new PictureBox();
            pic010Eur = new PictureBox();
            pic005eur = new PictureBox();
            pic002Eur = new PictureBox();
            pic001Eur = new PictureBox();
            btnCard = new Button();
            btnCash = new Button();
            pictureBox1 = new PictureBox();
            pictureBox2 = new PictureBox();
            pictureBox3 = new PictureBox();
            btnCancelOrder = new Button();
            ((System.ComponentModel.ISupportInitialize)picVisa).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic2Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picUnionPay).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picApplePay).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picGooglePay).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picMastercard).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic1Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic050Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic020Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic010Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic005eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic002Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pic001Eur).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            SuspendLayout();
            // 
            // lblPaymentSubtitle
            // 
            lblPaymentSubtitle.AutoSize = true;
            lblPaymentSubtitle.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblPaymentSubtitle.Location = new Point(48, 161);
            lblPaymentSubtitle.Name = "lblPaymentSubtitle";
            lblPaymentSubtitle.Size = new Size(80, 24);
            lblPaymentSubtitle.TabIndex = 15;
            lblPaymentSubtitle.Text = "Subtitle";
            // 
            // lblPaymentTitle
            // 
            lblPaymentTitle.AutoSize = true;
            lblPaymentTitle.Font = new Font("Arial", 38.25F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblPaymentTitle.Location = new Point(42, 101);
            lblPaymentTitle.Name = "lblPaymentTitle";
            lblPaymentTitle.Size = new Size(244, 60);
            lblPaymentTitle.TabIndex = 14;
            lblPaymentTitle.Text = "Main title";
            // 
            // lblPaymentCart
            // 
            lblPaymentCart.AutoSize = true;
            lblPaymentCart.Font = new Font("Arial", 15.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lblPaymentCart.Location = new Point(48, 215);
            lblPaymentCart.Name = "lblPaymentCart";
            lblPaymentCart.Size = new Size(86, 24);
            lblPaymentCart.TabIndex = 16;
            lblPaymentCart.Text = "Subtitle";
            // 
            // btnBack
            // 
            btnBack.AutoSize = true;
            btnBack.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnBack.BackColor = Color.FromArgb(241, 241, 241);
            btnBack.Cursor = Cursors.Hand;
            btnBack.FlatAppearance.BorderSize = 0;
            btnBack.FlatAppearance.MouseOverBackColor = Color.FromArgb(230, 230, 230);
            btnBack.FlatStyle = FlatStyle.Flat;
            btnBack.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnBack.Location = new Point(48, 574);
            btnBack.Name = "btnBack";
            btnBack.Padding = new Padding(20, 5, 20, 5);
            btnBack.Size = new Size(135, 44);
            btnBack.TabIndex = 23;
            btnBack.Text = "btnBack";
            btnBack.UseVisualStyleBackColor = false;
            // 
            // lblPaymentCards
            // 
            lblPaymentCards.AutoSize = true;
            lblPaymentCards.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblPaymentCards.Location = new Point(499, 215);
            lblPaymentCards.Name = "lblPaymentCards";
            lblPaymentCards.Size = new Size(172, 24);
            lblPaymentCards.TabIndex = 24;
            lblPaymentCards.Text = "lblPaymentCards";
            // 
            // lblPaymentCoins
            // 
            lblPaymentCoins.AutoSize = true;
            lblPaymentCoins.Font = new Font("Arial", 15.75F, FontStyle.Italic, GraphicsUnit.Point, 0);
            lblPaymentCoins.Location = new Point(499, 423);
            lblPaymentCoins.Name = "lblPaymentCoins";
            lblPaymentCoins.Size = new Size(170, 24);
            lblPaymentCoins.TabIndex = 25;
            lblPaymentCoins.Text = "lblPaymentCoins";
            // 
            // picVisa
            // 
            picVisa.Image = Resources.Assets.visa;
            picVisa.Location = new Point(499, 252);
            picVisa.Name = "picVisa";
            picVisa.Size = new Size(79, 50);
            picVisa.SizeMode = PictureBoxSizeMode.Zoom;
            picVisa.TabIndex = 26;
            picVisa.TabStop = false;
            // 
            // pic2Eur
            // 
            pic2Eur.Image = Resources.Assets._2eur;
            pic2Eur.Location = new Point(499, 450);
            pic2Eur.Name = "pic2Eur";
            pic2Eur.Size = new Size(55, 50);
            pic2Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic2Eur.TabIndex = 27;
            pic2Eur.TabStop = false;
            // 
            // picUnionPay
            // 
            picUnionPay.Image = Resources.Assets.unionpay;
            picUnionPay.Location = new Point(904, 252);
            picUnionPay.Name = "picUnionPay";
            picUnionPay.Size = new Size(79, 50);
            picUnionPay.SizeMode = PictureBoxSizeMode.Zoom;
            picUnionPay.TabIndex = 28;
            picUnionPay.TabStop = false;
            // 
            // picApplePay
            // 
            picApplePay.Image = Resources.Assets.applepay;
            picApplePay.Location = new Point(802, 252);
            picApplePay.Name = "picApplePay";
            picApplePay.Size = new Size(93, 50);
            picApplePay.SizeMode = PictureBoxSizeMode.Zoom;
            picApplePay.TabIndex = 29;
            picApplePay.TabStop = false;
            // 
            // picGooglePay
            // 
            picGooglePay.Image = Resources.Assets.googlepay;
            picGooglePay.Location = new Point(679, 252);
            picGooglePay.Name = "picGooglePay";
            picGooglePay.Size = new Size(111, 50);
            picGooglePay.SizeMode = PictureBoxSizeMode.Zoom;
            picGooglePay.TabIndex = 30;
            picGooglePay.TabStop = false;
            // 
            // picMastercard
            // 
            picMastercard.Image = Resources.Assets.mastercard;
            picMastercard.Location = new Point(588, 252);
            picMastercard.Name = "picMastercard";
            picMastercard.Size = new Size(79, 50);
            picMastercard.SizeMode = PictureBoxSizeMode.Zoom;
            picMastercard.TabIndex = 31;
            picMastercard.TabStop = false;
            // 
            // pic1Eur
            // 
            pic1Eur.Image = Resources.Assets._1eur;
            pic1Eur.Location = new Point(560, 450);
            pic1Eur.Name = "pic1Eur";
            pic1Eur.Size = new Size(55, 50);
            pic1Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic1Eur.TabIndex = 32;
            pic1Eur.TabStop = false;
            // 
            // pic050Eur
            // 
            pic050Eur.Image = Resources.Assets._050eur;
            pic050Eur.Location = new Point(621, 450);
            pic050Eur.Name = "pic050Eur";
            pic050Eur.Size = new Size(55, 50);
            pic050Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic050Eur.TabIndex = 33;
            pic050Eur.TabStop = false;
            // 
            // pic020Eur
            // 
            pic020Eur.Image = Resources.Assets._020eur;
            pic020Eur.Location = new Point(682, 450);
            pic020Eur.Name = "pic020Eur";
            pic020Eur.Size = new Size(55, 50);
            pic020Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic020Eur.TabIndex = 34;
            pic020Eur.TabStop = false;
            // 
            // pic010Eur
            // 
            pic010Eur.Image = Resources.Assets._010eur;
            pic010Eur.Location = new Point(743, 450);
            pic010Eur.Name = "pic010Eur";
            pic010Eur.Size = new Size(55, 50);
            pic010Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic010Eur.TabIndex = 35;
            pic010Eur.TabStop = false;
            // 
            // pic005eur
            // 
            pic005eur.Image = Resources.Assets._005eur;
            pic005eur.Location = new Point(804, 450);
            pic005eur.Name = "pic005eur";
            pic005eur.Size = new Size(55, 50);
            pic005eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic005eur.TabIndex = 36;
            pic005eur.TabStop = false;
            // 
            // pic002Eur
            // 
            pic002Eur.Image = Resources.Assets._002eur;
            pic002Eur.Location = new Point(867, 450);
            pic002Eur.Name = "pic002Eur";
            pic002Eur.Size = new Size(55, 50);
            pic002Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic002Eur.TabIndex = 37;
            pic002Eur.TabStop = false;
            // 
            // pic001Eur
            // 
            pic001Eur.Image = Resources.Assets._001eur;
            pic001Eur.Location = new Point(928, 450);
            pic001Eur.Name = "pic001Eur";
            pic001Eur.Size = new Size(55, 50);
            pic001Eur.SizeMode = PictureBoxSizeMode.Zoom;
            pic001Eur.TabIndex = 38;
            pic001Eur.TabStop = false;
            // 
            // btnCard
            // 
            btnCard.AutoSize = true;
            btnCard.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnCard.BackColor = Color.FromArgb(0, 196, 180);
            btnCard.Cursor = Cursors.Hand;
            btnCard.FlatAppearance.BorderSize = 0;
            btnCard.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnCard.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnCard.FlatStyle = FlatStyle.Flat;
            btnCard.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnCard.Location = new Point(499, 329);
            btnCard.Name = "btnCard";
            btnCard.Padding = new Padding(20, 5, 20, 5);
            btnCard.Size = new Size(133, 44);
            btnCard.TabIndex = 39;
            btnCard.Text = "btnCard";
            btnCard.UseVisualStyleBackColor = false;
            // 
            // btnCash
            // 
            btnCash.AutoSize = true;
            btnCash.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnCash.BackColor = Color.FromArgb(0, 196, 180);
            btnCash.Cursor = Cursors.Hand;
            btnCash.FlatAppearance.BorderSize = 0;
            btnCash.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 220, 200);
            btnCash.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 210, 190);
            btnCash.FlatStyle = FlatStyle.Flat;
            btnCash.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnCash.Location = new Point(499, 574);
            btnCash.Name = "btnCash";
            btnCash.Padding = new Padding(20, 5, 20, 5);
            btnCash.Size = new Size(137, 44);
            btnCash.TabIndex = 40;
            btnCash.Text = "btnCash";
            btnCash.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Resources.Assets._5eur;
            pictureBox1.Location = new Point(499, 506);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(102, 50);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 41;
            pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            pictureBox2.Image = Resources.Assets._10eur;
            pictureBox2.Location = new Point(607, 506);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(100, 50);
            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.TabIndex = 42;
            pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            pictureBox3.Image = Resources.Assets._20eur;
            pictureBox3.Location = new Point(713, 506);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(98, 50);
            pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox3.TabIndex = 43;
            pictureBox3.TabStop = false;
            // 
            // btnCancelOrder
            // 
            btnCancelOrder.AutoSize = true;
            btnCancelOrder.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnCancelOrder.BackColor = Color.FromArgb(255, 192, 192);
            btnCancelOrder.Cursor = Cursors.Hand;
            btnCancelOrder.FlatAppearance.BorderSize = 0;
            btnCancelOrder.FlatAppearance.MouseDownBackColor = Color.FromArgb(255, 140, 140);
            btnCancelOrder.FlatAppearance.MouseOverBackColor = Color.FromArgb(255, 128, 128);
            btnCancelOrder.FlatStyle = FlatStyle.Flat;
            btnCancelOrder.Font = new Font("Arial", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btnCancelOrder.Location = new Point(48, 635);
            btnCancelOrder.Name = "btnCancelOrder";
            btnCancelOrder.Padding = new Padding(20, 5, 20, 5);
            btnCancelOrder.Size = new Size(206, 44);
            btnCancelOrder.TabIndex = 44;
            btnCancelOrder.Text = "btnCancelOrder";
            btnCancelOrder.UseVisualStyleBackColor = false;
            // 
            // TicketMachinePaymentView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1008, 729);
            Controls.Add(btnCancelOrder);
            Controls.Add(pictureBox3);
            Controls.Add(pictureBox2);
            Controls.Add(pictureBox1);
            Controls.Add(btnCash);
            Controls.Add(btnCard);
            Controls.Add(pic001Eur);
            Controls.Add(pic002Eur);
            Controls.Add(pic005eur);
            Controls.Add(pic010Eur);
            Controls.Add(pic020Eur);
            Controls.Add(pic050Eur);
            Controls.Add(pic1Eur);
            Controls.Add(picMastercard);
            Controls.Add(picGooglePay);
            Controls.Add(picApplePay);
            Controls.Add(picUnionPay);
            Controls.Add(pic2Eur);
            Controls.Add(picVisa);
            Controls.Add(lblPaymentCoins);
            Controls.Add(lblPaymentCards);
            Controls.Add(btnBack);
            Controls.Add(lblPaymentCart);
            Controls.Add(lblPaymentSubtitle);
            Controls.Add(lblPaymentTitle);
            Name = "TicketMachinePaymentView";
            Text = "TicketMachinePaymentView";
            Controls.SetChildIndex(lblPaymentTitle, 0);
            Controls.SetChildIndex(lblPaymentSubtitle, 0);
            Controls.SetChildIndex(lblPaymentCart, 0);
            Controls.SetChildIndex(btnBack, 0);
            Controls.SetChildIndex(lblPaymentCards, 0);
            Controls.SetChildIndex(lblPaymentCoins, 0);
            Controls.SetChildIndex(picVisa, 0);
            Controls.SetChildIndex(pic2Eur, 0);
            Controls.SetChildIndex(picUnionPay, 0);
            Controls.SetChildIndex(picApplePay, 0);
            Controls.SetChildIndex(picGooglePay, 0);
            Controls.SetChildIndex(picMastercard, 0);
            Controls.SetChildIndex(pic1Eur, 0);
            Controls.SetChildIndex(pic050Eur, 0);
            Controls.SetChildIndex(pic020Eur, 0);
            Controls.SetChildIndex(pic010Eur, 0);
            Controls.SetChildIndex(pic005eur, 0);
            Controls.SetChildIndex(pic002Eur, 0);
            Controls.SetChildIndex(pic001Eur, 0);
            Controls.SetChildIndex(btnCard, 0);
            Controls.SetChildIndex(btnCash, 0);
            Controls.SetChildIndex(pictureBox1, 0);
            Controls.SetChildIndex(pictureBox2, 0);
            Controls.SetChildIndex(pictureBox3, 0);
            Controls.SetChildIndex(btnCancelOrder, 0);
            ((System.ComponentModel.ISupportInitialize)picVisa).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic2Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)picUnionPay).EndInit();
            ((System.ComponentModel.ISupportInitialize)picApplePay).EndInit();
            ((System.ComponentModel.ISupportInitialize)picGooglePay).EndInit();
            ((System.ComponentModel.ISupportInitialize)picMastercard).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic1Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic050Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic020Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic010Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic005eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic002Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pic001Eur).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblPaymentSubtitle;
        private Label lblPaymentTitle;
        private Label lblPaymentCart;
        private Button btnBack;
        private Label lblPaymentCards;
        private Label lblPaymentCoins;
        private PictureBox picVisa;
        private PictureBox pic2Eur;
        private PictureBox picUnionPay;
        private PictureBox picApplePay;
        private PictureBox picGooglePay;
        private PictureBox picMastercard;
        private PictureBox pic1Eur;
        private PictureBox pic050Eur;
        private PictureBox pic020Eur;
        private PictureBox pic010Eur;
        private PictureBox pic005eur;
        private PictureBox pic002Eur;
        private PictureBox pic001Eur;
        private Button btnCard;
        private Button btnCash;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private Button btnCancelOrder;
    }
}