﻿/// ETML
/// Author: Sebastien TILLE
/// Date: May 2nd, 2024

using TicketMachine.Utils;

namespace TicketMachine.Views
{
    /// <summary>
    /// Home view of the ticket machine program. Lets the user decide between
    /// recharching their Navigo Pass or buying a ticket.
    /// </summary>
    public partial class TicketMachineHomeView : TicketMachineEmptyBaseView
    {
        /// <summary>
        /// Initializes a new <see cref="TicketMachineHomeView"/>.
        /// </summary>
        public TicketMachineHomeView() : base()
        {
            InitializeComponent();

            lblHomeTicket.Click += CardText_Click;
            picTicket.Click += CardBackground_Click;
        }

        #region Events
        /// <summary>
        /// Handles click interractions with cards (Navigo pass or ticket
        /// selection options).
        /// </summary>
        /// <param name="sender">The associated card.</param>
        /// <param name="e">A click event.</param>
        /// /// <exception cref="ArgumentException">If the sender is invalid.
        /// </exception>
        /// <exception cref="NotImplementedException">If the card is invalid.
        /// </exception>
        private void CardBackground_Click(object? sender, EventArgs e)
        {
            if (sender is not PictureBox background)
                throw new ArgumentException("Invalid card background!", 
                                            nameof(sender));

            ViewType that = background.Name switch
            {
                "picTicket" => ViewType.Selection,
                _ => throw new NotImplementedException(nameof(background))
            };

            Controller.OpenView(this, that);
        }

        /// <summary>
        /// Handles click interractions with cards (Navigo pass or ticket
        /// selection options).
        /// </summary>
        /// <param name="sender">The associated card.</param>
        /// <param name="e">A click event.</param>
        /// <exception cref="ArgumentException">If the sender is invalid.
        /// </exception>
        /// <exception cref="NotImplementedException">If the title is invalid.
        /// </exception>
        private void CardText_Click(object? sender, EventArgs e)
        {
            if (sender is not Label text)
                throw new ArgumentException("Invalid card title!", 
                                            nameof(sender));

            ViewType that = text.Name switch
            {
                "lblHomeTicket" => ViewType.Selection,
                _ => throw new NotImplementedException(nameof(text))
            };

            Controller.OpenView(this, that);
        }
        #endregion
    }
}
